use recipes;
drop database recipes;

CREATE DATABASE IF NOT EXISTS `recipes` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
USE `recipes`;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `recipes`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `main_category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `categories`
--

INSERT INTO `categories` (`id`, `name`, `main_category_id`) VALUES
(1, 'Śniadania', 1),
(2, 'Lunche', 2),
(3, 'Zupy', 3),
(4, 'Sałatki', 4),
(5, 'Surówki', 5),
(6, 'Makarony', 6),
(7, 'Dania główne', 7),
(8, 'Podwieczorki', 8),
(9, 'Napoje', 9),
(10, 'Kolacje', 10),
(11, 'Desery', 11),
(12, 'Kotlety', 7),
(13, 'Dania z kaszą', 7),
(14, 'Kasza gryczana', 7),
(15, 'Kasza jęczmienna', 7),
(16, 'Kasza kus kus', 7),
(17, 'Dania z mięsem', 7),
(18, 'Dania duszone', 7),
(19, 'Indyk', 7),
(20, 'Indyk pieczony', 7),
(21, 'Pierś z indyka', 7),
(22, 'Kaczka', 7),
(23, 'Kaczka pieczona', 7),
(24, 'Pierś z kaczki', 7),
(25, 'Kurczak', 7),
(26, 'Kurczak smażony', 7),
(27, 'Kurczak z ryżem', 7),
(28, 'Pierś z kurczaka', 7),
(29, 'Udka z kurczaka', 7),
(30, 'Mięsa mieszane', 7),
(31, 'Gyros', 7),
(32, 'Pasztet', 7),
(33, 'Pulpety', 7),
(34, 'Mięso wołowo-wieprzowe', 7),
(35, 'Pieczeń', 7),
(36, 'Roladki', 7),
(37, 'Wieprzowina', 7),
(38, 'Golonka', 7),
(39, 'Karkówka', 7),
(40, 'Karkówka pieczona', 7),
(41, 'Karkówka smażona', 7),
(42, 'Polędwiczki wieprzowe', 7),
(43, 'Schab', 7),
(44, 'Schab faszerowany', 7),
(45, 'Schab pieczony', 7),
(46, 'Schab smażony', 7),
(47, 'Szynka pieczona', 7),
(48, 'Żeberka', 7),
(49, 'Żeberka duszone', 7),
(50, 'Żeberka pieczone', 7),
(51, 'Wołowina', 7),
(52, 'Polędwiczki wołowe', 7),
(53, 'Wołowina pieczona', 7),
(54, 'Zrazy', 7),
(55, 'Grzyby', 7),
(56, 'Dania z ryżem', 7),
(57, 'Ryż z kurczakiem', 7),
(58, 'Ryż z warzywami', 7),
(59, 'Ryż wegetariański', 7),
(60, 'Faszerowane warzywa', 7),
(61, 'Dania z kapusty', 7),
(62, 'Ryba', 7),
(63, 'Karp', 7),
(64, 'Karp faszerowany', 7),
(65, 'Karp pieczony', 7),
(66, 'Karp po żydowsku', 7),
(67, 'Karp smażony', 7),
(68, 'Karp w galarecie', 7),
(69, 'Kotlety rybne', 7),
(70, 'Łosoś', 7),
(71, 'Pstrąg', 7),
(72, 'Ryba duszona', 7),
(73, 'Ryba pieczona', 7),
(74, 'Ryba po grecku', 7),
(75, 'Ryba smażona', 7),
(76, 'Ryba w galarecie', 7),
(77, 'Ryba z warzywami', 7),
(78, 'Śledzie', 7),
(79, 'Warzywa', 7),
(80, 'Soczewica', 7),
(81, 'Przetwory', 7),
(82, 'Dania mączne', 7),
(83, 'Kluski', 7),
(84, 'Kopytka', 7),
(85, 'Leniwe', 7),
(86, 'Knedle', 7),
(87, 'Pierogi', 7),
(88, 'Placki', 7),
(89, 'Placki po węgiersku', 7),
(90, 'Placki ziemniaczane', 7),
(91, 'Racuchy', 11),
(92, 'Jednogarnkowe', 7),
(93, 'Bigos', 7),
(94, 'Chili con carne', 7),
(95, 'Curry', 7),
(96, 'Gołąbki', 7),
(97, 'Gulasz', 7),
(98, 'Gulasz węgierski', 7),
(99, 'Gulasz wieprzowy', 7),
(100, 'Gulasz wołowy', 7),
(101, 'Kaszotto', 7),
(102, 'Leczo', 7),
(103, 'Risotto', 7),
(104, 'Strogonow', 7),
(105, 'Kanapki', 1),
(106, 'Kanapki na ciepło', 1),
(107, 'Kanapki na lunch', 2),
(108, 'Kanapki na przyjęcie', 8),
(109, 'Kanapki na śniadanie', 1),
(110, 'Pasty do kanapek', 1),
(111, 'Krokiety', 7),
(113, 'Cannelloni', 6),
(114, 'Lasagne', 6),
(115, 'Łazanki', 6),
(116, 'Makaron bezmięsny', 6),
(117, 'Makaron po chińsku', 6),
(118, 'Makaron ryżowy', 6),
(119, 'Makaron z mięsem', 6),
(120, 'Makaron z kurczakiem', 6),
(121, 'Makaron z mięsem mielonym', 6),
(122, 'Makaron z rybą', 6),
(123, 'Makaron z łososiem', 6),
(124, 'Makaron z tuńczykiem', 6),
(125, 'Makaron z warzywami', 6),
(126, 'Makaron z brokułami', 6),
(127, 'Makaron ze szpinakiem', 6),
(128, 'Makarony z sosem', 6),
(129, 'Makaron z sosem grzybowym', 6),
(130, 'Makaron z sosem pomidorowym', 6),
(131, 'Makaron z sosem serowym', 6),
(132, 'Makaron z sosem śmietanowym', 6),
(133, 'Penne', 6),
(134, 'Spaghetti', 6),
(135, 'Spaghetti bolognese', 6),
(136, 'Spaghetti carbonara', 6),
(137, 'Tagliatelle', 6),
(138, 'Naleśniki', 1),
(139, 'Naleśniki z mięsem', 7),
(140, 'Naleśniki z serem', 8),
(141, 'Naleśniki z warzywami', 7),
(142, 'Pizze', 7),
(143, 'Przystawki', 8),
(144, 'Frytki', 7),
(145, 'Jajka', 1),
(146, 'Jajecznice', 1),
(147, 'Jajka faszerowane', 8),
(148, 'Omlety', 1),
(149, 'Meksykańskie przekąski', 8),
(150, 'Nuggetsy', 7),
(151, 'Paszteciki', 7),
(152, 'Pasztety', 7),
(153, 'Szaszłyki', 10),
(154, 'Śledzie na różne sposoby', 7),
(155, 'Tapas', 10),
(156, 'Włoskie przekąski', 8),
(157, 'Warzywa na ciepło', 7),
(158, 'Sałatki greckie', 4),
(159, 'Sałatki śledziowe', 4),
(160, 'Sałatki z cykorią', 4),
(161, 'Sałatki z jajkiem', 4),
(162, 'Sałatki z kapustą pekińską', 4),
(163, 'Sałatki z makaronem', 4),
(164, 'Sałatki z mięsem', 4),
(165, 'Sałatki gyros', 4),
(166, 'Sałatki z indykiem', 4),
(167, 'Sałatki z kurczakiem', 4),
(168, 'Sałatki z owocami', 4),
(169, 'Sałatki z roszponką', 4),
(170, 'Sałatki z rukolą', 4),
(171, 'Sałatki z rybą', 4),
(172, 'Sałatki z ryżem', 4),
(173, 'Sałatki z warzywami', 4),
(174, 'Sałatki z brokułami', 4),
(175, 'Sałatki z dynią', 4),
(176, 'Sałatki z kalafiorem', 4),
(177, 'Sałatki z ogórkiem', 4),
(178, 'Sałatki z papryką', 4),
(179, 'Sałatki ze szpinakiem', 4),
(180, 'Sosy', 7),
(181, 'Dipy', 7),
(182, 'Sosy do dań', 7),
(183, 'Sos ciemny', 7),
(184, 'Sos jasny', 7),
(185, 'Street food', 7),
(186, 'Burgery', 7),
(187, 'Kebab', 7),
(188, 'Tortille i wrapy', 7),
(189, 'Surówki', 5),
(190, 'Zapiekanki', 7),
(191, 'Zapiekanka z makaronem', 6),
(192, 'Zapiekanka z mięsem', 7),
(193, 'Zapiekanka z kurczakiem', 7),
(194, 'Zapiekanka z mięsem mielonym', 7),
(195, 'Zapiekanka z ryżem', 7),
(196, 'Zapiekanka z warzyw', 7),
(197, 'Zapiekanka z brokułów', 7),
(198, 'Zapiekanka z kalafiora', 7),
(199, 'Zapiekanka z ziemniakami', 7),
(200, 'Zupy kremy', 3),
(201, 'Zupy orientalne', 3),
(202, 'Zupy polskie', 3),
(203, 'Barszcz biały', 3),
(204, 'Barszcz czerwony', 3),
(205, 'Chłodniki', 3),
(206, 'Flaki', 3),
(207, 'Grochówka', 3),
(208, 'Krupniki', 3),
(209, 'Ogórkowa', 3),
(210, 'Pomidorowa', 3),
(211, 'Rosół', 3),
(212, 'Zupa grzybowa', 3),
(213, 'Zupa gulaszowa', 3),
(214, 'Zupa jarzynowa', 3),
(215, 'Zupa rybna', 3),
(216, 'Żurek', 3),
(217, 'Babka piaskowa', 11),
(218, 'Bezy ', 11),
(219, 'Biszkopty', 11),
(220, 'Biszkopty czekoladowe', 11),
(221, 'Biszkopty z galaretką', 11),
(222, 'Biszkopty z kremem', 11),
(223, 'Biszkopty z owocami', 11),
(224, 'Keks', 11),
(225, 'Ciasta bez pieczenia', 11),
(226, 'Tiramisu', 11),
(227, 'Wafle i andruty', 11),
(228, 'Ciasta bezglutenowe', 11),
(229, 'Ciasta czekoladowe', 11),
(230, 'Brownie', 11),
(231, 'Ciasta z białą czekoladą', 11),
(232, 'Murzynki', 11),
(233, 'Ciasta drożdżowe', 11),
(234, 'Bułeczki drożdżowe', 11),
(235, 'Rogaliki drożdżowe', 11),
(236, 'Ciasta francuskie', 11),
(237, 'Ciasta parzone', 11),
(238, 'Ciasta ucierane', 11),
(239, 'Ciasta z galaretką', 11),
(240, 'Ciasta z kremem', 11),
(241, 'Ciasta z lodami', 11),
(242, 'Ciasta z makiem', 11),
(243, 'Ciasta z owocami', 11),
(244, 'Ciasta bananowe', 11),
(245, 'Ciasta ze śliwkami', 11),
(246, 'Ciasta z brzoskwiniami', 11),
(247, 'Ciasta z gruszką', 11),
(248, 'Ciasta z jabłkami', 11),
(249, 'Ciasta z jagodami', 11),
(250, 'Ciasta z malinami', 11),
(251, 'Ciasta z truskawkami', 11),
(252, 'Ciasta z wiśniami', 11),
(253, 'Ciasto z borówkami', 11),
(254, 'Ciasto z porzeczkami', 11),
(255, 'Ciasta z warzywami', 11),
(256, 'Ciasta marchewkowe', 11),
(257, 'Ciasta z rabarbarem', 11),
(258, 'Ciasteczka', 11),
(259, 'Ciasteczka kruche', 11),
(260, 'Ciasteczka owsiane', 11),
(261, 'Ciasteczka z czekoladą', 11),
(262, 'Faworki i chrust', 11),
(263, 'Kokosanki', 11),
(264, 'Rogaliki', 11),
(265, 'Gofry', 11),
(266, 'Kruche ciasta', 11),
(267, 'Mazurki', 11),
(268, 'Mazurki kajmakowe', 11),
(269, 'Szarlotki (ciasta z jabłkami, jabłeczniki)', 11),
(270, 'Tarty', 11),
(271, 'Tarta na słodko', 11),
(272, 'Tarta na słono', 11),
(273, 'Tartaletki', 11),
(274, 'Muffinki i babeczki', 11),
(275, 'Babeczki wytrawne', 11),
(276, 'Babeczki z kremem (Cupcakes)', 11),
(277, 'Muffinki czekoladowe', 11),
(278, 'Muffinki z owocami', 11),
(279, 'Pierniczki', 11),
(280, 'Pierniki', 11),
(281, 'Pączki, oponki, donaty', 11),
(282, 'Serniki', 11),
(283, 'Serniki na zimno', 11),
(284, 'Serniki pieczone', 11),
(285, 'Torty', 11),
(286, 'Torty z owocami', 11),
(287, 'Świąteczne desery', 11),
(288, 'Ciasta imieninowe i urodzinowe', 11),
(289, 'Lody i napoje', 11),
(290, 'Ciasta z lodami', 11),
(291, 'Desery lodowe', 11),
(292, 'Napoje ciepłe', 9),
(293, 'Napoje zimne', 9),
(294, 'Mrożone herbaty', 9),
(295, 'Smoothie/ koktajle', 9),
(296, 'Dania wegańskie', 7),
(297, 'Śniadanie wegańskie', 1),
(298, 'Zupy wegańskie', 3),
(299, 'Obiad wegański', 7),
(300, 'Dania wegetariańskie', 7),
(301, 'Przekąski i dodatki wegetariańskie', 8),
(302, 'Śniadanie wegetariańskie', 1),
(303, 'Sałatki wegetariańskie', 4),
(304, 'Zupy wegetariańskie', 3),
(305, 'Obiad wegetariański', 7),
(306, 'Makaron i ryż wegetariański', 6),
(307, 'Bez laktozy', 7),
(308, 'Ciasta bez laktozy', 11),
(309, 'Śniadanie bez laktozy', 1),
(310, 'Obiad bez laktozy', 7),
(311, 'Zdrowe obiady', 7),
(312, 'Grill', 7),
(313, 'Drób z grilla', 7),
(314, 'Karkówka z grilla', 7),
(315, 'Ryby z grilla', 7),
(316, 'Sałatki na grilla', 7),
(317, 'Sosy do mięs na grilla ', 7),
(318, 'Steki z grilla', 7),
(319, 'Stek z wieprzowiny z grilla', 7),
(320, 'Stek z wołowiny z grilla', 7),
(321, 'Szaszłyki z grilla', 7),
(322, 'Warzywa z grilla', 7),
(323, 'Żeberka z grilla', 7),
(324, 'Grill&Travel', 7),
(325, 'Grill wegetariański ', 7),
(326, 'Grill na słodko', 7),
(327, 'Grill pikantny/BBQ ', 7),
(328, 'Grill ziołowy', 7),
(329, 'Sylwester', 7),
(330, 'Wigilia i Święta', 7),
(331, 'Grzyby, kapusta i pierogi', 7),
(332, 'Ryby na Wigilię', 7),
(333, 'Śledzie na Wigilię', 7),
(334, 'Zupy na święta', 3),
(335, 'Mięsa i sosy na Boże Narodzenie', 7),
(336, 'Sałatki na święta', 4),
(337, 'Ciasta i ciasteczka na święta', 11),
(338, 'Przystawki na święta', 8),
(339, 'Wielkanoc', 7),
(340, 'Mięsa i sosy na Wielkanoc', 7),
(341, 'Przystawki na Wielkanoc', 7),
(342, 'Sałatki na Wielkanoc', 4),
(343, 'Zupy na Wielkanoc', 3),
(344, 'Babki wielkanocne', 11),
(345, 'Mazurki', 11),
(346, 'Ciasta wielkanocne', 11),
(347, 'Karnawał', 7),
(348, 'Dania na ciepło na karnawał', 7),
(349, 'Przekąski na karnawał', 8),
(350, 'Ciasta na Karnawał', 11),
(351, 'Tłusty Czwartek', 11),
(352, 'Walentynki', 11),
(353, 'Dania do pracy', 7),
(354, 'Szybkie śniadanie', 1),
(355, 'Obiad z mięsa mielonego', 7),
(356, 'Obiad z kurczakiem', 7),
(357, 'Obiad z makaronem', 6),
(358, 'Obiad z ryżem', 7),
(359, 'Obiad z kaszą', 7),
(360, 'Szybki obiad', 7),
(361, 'Szybka kolacja', 10),
(362, 'Szybki deser', 11),
(363, 'Przekąski', 8),
(364, 'Kuchnia Bliskiego Wschodu', 7),
(365, 'Kuchnia amerykańska', 7),
(366, 'Kuchnia bałkańska', 7),
(367, 'Kuchnia chińska', 7),
(368, 'Kuchnia francuska', 7),
(369, 'Kuchnia fusion', 7),
(370, 'Kuchnia grecka', 7),
(371, 'Kuchnia gruzińska', 7),
(372, 'Kuchnia hiszpańska', 7),
(373, 'Kuchnia indyjska', 7),
(374, 'Kuchnia koreańska', 7),
(375, 'Kuchnia meksykańska', 7),
(376, 'Kuchnia niemiecka', 7),
(377, 'Kuchnia orientalna', 7),
(378, 'Kuchnia polska', 7),
(379, 'Kuchnia Północnej Afryki', 7),
(380, 'Kuchnia rosyjska', 7),
(381, 'Kuchnia śródziemnomorska', 7),
(382, 'Kuchnia tajska', 7),
(383, 'Kuchnia turecka', 7),
(384, 'Kuchnia węgierska', 7),
(385, 'Kuchnia włoska', 7),
(9999, 'kategoria', 9999);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `comment`
--

CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `comment_id` bigint(20) DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `databasechangelog`
--

CREATE TABLE `databasechangelog` (
  `ID` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `AUTHOR` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `FILENAME` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) COLLATE utf8_polish_ci NOT NULL,
  `MD5SUM` varchar(35) COLLATE utf8_polish_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `COMMENTS` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `LIQUIBASE` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  `CONTEXTS` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `LABELS` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `databasechangelog`
--

INSERT INTO `databasechangelog` (`ID`, `AUTHOR`, `FILENAME`, `DATEEXECUTED`, `ORDEREXECUTED`, `EXECTYPE`, `MD5SUM`, `DESCRIPTION`, `COMMENTS`, `TAG`, `LIQUIBASE`, `CONTEXTS`, `LABELS`, `DEPLOYMENT_ID`) VALUES
('00000000000001', 'jhipster', 'config/liquibase/changelog/00000000000000_initial_schema.xml', '2019-04-11 16:48:31', 1, 'EXECUTED', '7:5277bf8eda377b65febe13e8b1dfd4a9', 'createTable tableName=jhi_user; createTable tableName=jhi_authority; createTable tableName=jhi_user_authority; addPrimaryKey tableName=jhi_user_authority; addForeignKeyConstraint baseTableName=jhi_user_authority, constraintName=fk_authority_name, ...', '', NULL, '3.5.4', NULL, NULL, '4994111354');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `databasechangeloglock`
--

CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `databasechangeloglock`
--

INSERT INTO `databasechangeloglock` (`ID`, `LOCKED`, `LOCKGRANTED`, `LOCKEDBY`) VALUES
(1, b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ingredient`
--

CREATE TABLE `ingredient` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `ingredient`
--

INSERT INTO `ingredient` (`id`, `name`, `description`) VALUES
(1, 'Alkohol', NULL),
(2, 'Amaretto', NULL),
(3, 'Ananas', NULL),
(4, 'Arbuz', NULL),
(5, 'Awokado', NULL),
(6, 'Babka wielkanocna', NULL),
(7, 'Banan', NULL),
(8, 'Baranina', NULL),
(9, 'Bazylia', NULL),
(10, 'Bezy', NULL),
(11, 'Białe wino', NULL),
(12, 'Białka', NULL),
(13, 'Biały rum', NULL),
(14, 'Biszkopty', NULL),
(15, 'Boczek', NULL),
(16, 'Brandy', NULL),
(17, 'Brokuł', NULL),
(18, 'Brokuły', NULL),
(19, 'Budyń śmietankowy', NULL),
(20, 'Burak liściasty', NULL),
(21, 'Buraki', NULL),
(22, 'Burgery', NULL),
(23, 'Bułka tarta', NULL),
(24, 'Cannelloni', NULL),
(25, 'Cebula', NULL),
(26, 'Cebula czerwona', NULL),
(27, 'Cebulki marynowane', NULL),
(28, 'Chałwa', NULL),
(29, 'Chrzan', NULL),
(30, 'Ciasteczka', NULL),
(31, 'Ciasto', NULL),
(32, 'Ciasto drożdżowe', NULL),
(33, 'Ciasto francuskie', NULL),
(34, 'Coste', NULL),
(35, 'Cukier', NULL),
(36, 'Cukier brązowy', NULL),
(37, 'Cukier puder', NULL),
(38, 'Cukier trzcinowy', NULL),
(39, 'Cukier waniliowy', NULL),
(40, 'Cukinia', NULL),
(41, 'Curry', NULL),
(42, 'Cynamon', NULL),
(43, 'Cytryna', NULL),
(44, 'Cząber', NULL),
(45, 'Czekolada', NULL),
(46, 'Czekolada mleczna', NULL),
(47, 'Czereśnie', NULL),
(48, 'Czerwona papryka', NULL),
(49, 'Czerwone wino', NULL),
(50, 'Czosnek', NULL),
(51, 'Dorsz', NULL),
(52, 'Draże', NULL),
(53, 'Drożdze', NULL),
(54, 'Dynia', NULL),
(55, 'Dżem porzeczkowy', NULL),
(56, 'Dżem wiśniowy', NULL),
(57, 'Dzika róża', NULL),
(58, 'Fasola', NULL),
(59, 'Feta', NULL),
(60, 'Figi', NULL),
(61, 'Galaretka', NULL),
(62, 'Golonka', NULL),
(63, 'Gorzka czekolada', NULL),
(64, 'Gościnna chata', NULL),
(65, 'Gouda', NULL),
(66, 'Grana padano', NULL),
(67, 'Grejpfrut', NULL),
(68, 'Groszek zielony', NULL),
(69, 'Gruszka', NULL),
(70, 'Grzyby', NULL),
(71, 'Grzyby suszone', NULL),
(72, 'Herbata', NULL),
(73, 'Herbatniki', NULL),
(74, 'Imbir', NULL),
(75, 'Indyk', NULL),
(76, 'Jabłka', NULL),
(77, 'Jagody', NULL),
(78, 'Jajka', NULL),
(79, 'Jeżyny', NULL),
(80, 'Jogurt grecki', NULL),
(81, 'Jogurt naturalny', NULL),
(82, 'Kabanosy', NULL),
(83, 'Kacze piersi', NULL),
(84, 'Kaczka', NULL),
(85, 'Kakao', NULL),
(86, 'Kalafior', NULL),
(87, 'Kalmary', NULL),
(88, 'Kapusta', NULL),
(89, 'Kapusta pekińska', NULL),
(90, 'Karczek wieprzowy', NULL),
(91, 'Kasza gryczana', NULL),
(92, 'Kasza jaglana', NULL),
(93, 'Kawa', NULL),
(94, 'Kawa inka', NULL),
(95, 'Kawa rozpuszczalna', NULL),
(96, 'Ketchup', NULL),
(97, 'Kiełbasa', NULL),
(98, 'Kiełbasa podwawelska', NULL),
(99, 'Kiwi', NULL),
(100, 'Kleik ryżowy', NULL),
(101, 'Kminek', NULL),
(102, 'Kolendra', NULL),
(103, 'Koncentrat pomidorowy', NULL),
(104, 'Konfitury', NULL),
(105, 'Koniak', NULL),
(106, 'Koper włoski', NULL),
(107, 'Koperek', NULL),
(108, 'Kołduny', NULL),
(109, 'Krem z gorczycy', NULL),
(110, 'Krewetki', NULL),
(111, 'Krewetki koktajlowe', NULL),
(112, 'Kruche ciasteczka', NULL),
(113, 'Kukurydza', NULL),
(114, 'Kurczak', NULL),
(115, 'Kurki', NULL),
(116, 'Kurkuma', NULL),
(117, 'Limonka', NULL),
(118, 'Liście laurowe', NULL),
(119, 'Lód', NULL),
(120, 'Lody', NULL),
(121, 'Lody waniliowe', NULL),
(122, 'Lubczyk', NULL),
(123, 'Łosoś', NULL),
(124, 'Majeranek', NULL),
(125, 'Majonez', NULL),
(126, 'Mąka', NULL),
(127, 'Mąka kasztanowa', NULL),
(128, 'Mąka kukurydziana', NULL),
(129, 'Mąka pszenna', NULL),
(130, 'Mąka ryżowa', NULL),
(131, 'Mąka tortowa', NULL),
(132, 'Mąka ziemniaczana', NULL),
(133, 'Mąka żytnia', NULL),
(134, 'Makaron', NULL),
(135, 'Makaron kokardki', NULL),
(136, 'Makaron penne', NULL),
(137, 'Makaron ryżowy', NULL),
(138, 'Makaron spaghetti', NULL),
(139, 'Makaron świderki', NULL),
(140, 'Makaron tagliatelle', NULL),
(141, 'Maliny', NULL),
(142, 'Marcepan', NULL),
(143, 'Marchew', NULL),
(144, 'Margaryna', NULL),
(145, 'Mascarpone', NULL),
(146, 'Masło', NULL),
(147, 'Masło orzechowe', NULL),
(148, 'Małże', NULL),
(149, 'Melon', NULL),
(150, 'Melon miodowy', NULL),
(151, 'Mięso mielone', NULL),
(152, 'Mięso wołowe', NULL),
(153, 'Mięta', NULL),
(154, 'Migdały', NULL),
(155, 'Miód', NULL),
(156, 'Miruna', NULL),
(157, 'Mlecz', NULL),
(158, 'Mleczko kokosowe', NULL),
(159, 'Mleko', NULL),
(160, 'Mozzarella', NULL),
(161, 'Muffiny', NULL),
(162, 'Musztarda', NULL),
(163, 'Nalewka', NULL),
(164, 'Natka pietruszki', NULL),
(165, 'Nutella', NULL),
(166, 'Ocet', NULL),
(167, 'Ocet balsamiczny', NULL),
(168, 'Ocet winny', NULL),
(169, 'Ogórek', NULL),
(170, 'Ogórek konserwowy', NULL),
(171, 'Olej', NULL),
(172, 'Olej kokosowy', NULL),
(173, 'Olej rzepakowy', NULL),
(174, 'Olej z pestek dyni', NULL),
(175, 'Olej z pestek winogron', NULL),
(176, 'Olejek waniliowy', NULL),
(177, 'Oliwa z oliwek', NULL),
(178, 'Oliwki czarne', NULL),
(179, 'Oliwki zielone', NULL),
(180, 'Oregano', NULL),
(181, 'Orzechy laskowe', NULL),
(182, 'Orzechy włoskie', NULL),
(183, 'Orzeszki ziemne', NULL),
(184, 'Ostra papryka', NULL),
(185, 'Otręby owsiane', NULL),
(186, 'Papryka', NULL),
(187, 'Papryka czerwona', NULL),
(188, 'Papryka zielona', NULL),
(189, 'Papryka żółta', NULL),
(190, 'Parmezan', NULL),
(191, 'Penne', NULL),
(192, 'Pesto', NULL),
(193, 'Pieczarki', NULL),
(194, 'Pieczarki marynowane', NULL),
(195, 'Pieczywo', NULL),
(196, 'Pieprz', NULL),
(197, 'Pierogi', NULL),
(198, 'Pierś z indyka', NULL),
(199, 'Pierś z kurczaka', NULL),
(200, 'Pietruszka', NULL),
(201, 'Pigwowiec japoński', NULL),
(202, 'Piwo', NULL),
(203, 'Pizza', NULL),
(204, 'Pomidorki koktajlowe', NULL),
(205, 'Pomidory', NULL),
(206, 'Por', NULL),
(207, 'Proszek do pieczenia', NULL),
(208, 'Przecier pomidorowy', NULL),
(209, 'Pstrąg', NULL),
(210, 'Płatki migdałowe', NULL),
(211, 'Płatki owsiane', NULL),
(212, 'Rodzynki', NULL),
(213, 'Rozmaryn', NULL),
(214, 'Rukola', NULL),
(215, 'Rum', NULL),
(216, 'Ryby', NULL),
(217, 'Ryż', NULL),
(218, 'Ryż preparowany', NULL),
(219, 'Rzeżucha', NULL),
(220, 'Rzodkiewka', NULL),
(221, 'Sałata lodowa', NULL),
(222, 'Sałata masłowa', NULL),
(223, 'Schab', NULL),
(224, 'Seler', NULL),
(225, 'Seler naciowy', NULL),
(226, 'Ser biały', NULL),
(227, 'Ser waniliowy', NULL),
(228, 'Ser żółty', NULL),
(229, 'Serek homogenizowany', NULL),
(230, 'Serek śmietankowy', NULL),
(231, 'Serek topiony', NULL),
(232, 'Serek waniliowy', NULL),
(233, 'Smalec', NULL),
(234, 'Soczewica', NULL),
(235, 'Soczewica czerwona', NULL),
(236, 'Soda oczyszczona', NULL),
(237, 'Sok wiśniowy', NULL),
(238, 'Sól', NULL),
(239, 'Sos barbecue', NULL),
(240, 'Sos musztardowy', NULL),
(241, 'Sos pieczarkowy', NULL),
(242, 'Sos śliwkowy', NULL),
(243, 'Spirytus', NULL),
(244, 'Surimi', NULL),
(245, 'Suszone pomidory', NULL),
(246, 'Syrop klonowy', NULL),
(247, 'Szczypiorek', NULL),
(248, 'Szparagi', NULL),
(249, 'Szpinak', NULL),
(250, 'Szynka', NULL),
(251, 'Szynka konserwowa', NULL),
(252, 'Szynka parmeńska', NULL),
(253, 'Szynka szwarcwaldzka', NULL),
(254, 'Szynka wędzona', NULL),
(255, 'Słonecznik', NULL),
(256, 'Śliwki kalifornijskie', NULL),
(257, 'Śliwki suszone', NULL),
(258, 'Śmietana', NULL),
(259, 'Śmietana kremówka', NULL),
(260, 'Tarty', NULL),
(261, 'Toffi', NULL),
(262, 'Truskawki', NULL),
(263, 'Tuńczyk', NULL),
(264, 'Twaróg', NULL),
(265, 'Tymianek', NULL),
(266, 'Wanilia', NULL),
(267, 'Whiskey', NULL),
(268, 'Wieprzowina', NULL),
(269, 'Wiórki kokosowe', NULL),
(270, 'Wiśnie', NULL),
(271, 'Woda', NULL),
(272, 'Woda mineralna', NULL),
(273, 'Wołowina', NULL),
(274, 'Ziele angielskie', NULL),
(275, 'Zielony groszek', NULL),
(276, 'Ziemniaki', NULL),
(277, 'Żeberka', NULL),
(278, 'Żelatyna', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jhi_authority`
--

CREATE TABLE `jhi_authority` (
  `name` varchar(50) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `jhi_authority`
--

INSERT INTO `jhi_authority` (`name`) VALUES
('ROLE_ADMIN'),
('ROLE_USER');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jhi_persistent_audit_event`
--

CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL,
  `principal` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `jhi_persistent_audit_event`
--

INSERT INTO `jhi_persistent_audit_event` (`event_id`, `principal`, `event_date`, `event_type`) VALUES
(1, 'admin', '2019-04-11 13:40:51', 'AUTHENTICATION_SUCCESS'),
(2, 'admin', '2019-04-17 06:07:23', 'AUTHENTICATION_SUCCESS'),
(3, 'admin', '2019-04-18 13:29:25', 'AUTHENTICATION_SUCCESS'),
(4, 'admin', '2019-04-18 13:39:26', 'AUTHENTICATION_SUCCESS'),
(5, 'admin', '2019-04-18 14:29:00', 'AUTHENTICATION_SUCCESS');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jhi_persistent_audit_evt_data`
--

CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) COLLATE utf8_polish_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jhi_user`
--

CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL,
  `login` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_polish_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_polish_ci DEFAULT NULL,
  `image_url` varchar(256) COLLATE utf8_polish_ci DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(6) COLLATE utf8_polish_ci DEFAULT NULL,
  `activation_key` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  `reset_key` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `created_date` timestamp NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `jhi_user`
--

INSERT INTO `jhi_user` (`id`, `login`, `password_hash`, `first_name`, `last_name`, `email`, `image_url`, `activated`, `lang_key`, `activation_key`, `reset_key`, `created_by`, `created_date`, `reset_date`, `last_modified_by`, `last_modified_date`) VALUES
(1, 'system', '$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG', 'System', 'System', 'system@localhost', '', b'1', 'pl', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(2, 'anonymoususer', '$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO', 'Anonymous', 'User', 'anonymous@localhost', '', b'1', 'pl', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(3, 'admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Administrator', 'Administrator', 'admin@localhost', '', b'1', 'pl', NULL, NULL, 'system', NULL, NULL, 'system', NULL),
(4, 'user', '$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K', 'User', 'User', 'user@localhost', '', b'1', 'pl', NULL, NULL, 'system', NULL, NULL, 'system', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jhi_user_authority`
--

CREATE TABLE `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `jhi_user_authority`
--

INSERT INTO `jhi_user_authority` (`user_id`, `authority_name`) VALUES
(1, 'ROLE_ADMIN'),
(1, 'ROLE_USER'),
(3, 'ROLE_ADMIN'),
(3, 'ROLE_USER'),
(4, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `keyword`
--

CREATE TABLE `keyword` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `measure`
--

CREATE TABLE `measure` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `measure`
--

INSERT INTO `measure` (`id`, `type`, `description`) VALUES
(1, 'gram', NULL),
(2, 'kilogram', NULL),
(3, 'sztuka', NULL),
(4, 'szczypta', NULL),
(5, 'łyżka', 'Około 15 mililitrów lub 14 gramów'),
(6, 'szklanka', 'Około 250 mililitrów lub 230 gramów'),
(7, 'dekagram', NULL),
(8, 'litr', 'Około 4 szklanki'),
(9, 'pęczek', NULL),
(10, 'opakowanie', NULL),
(11, 'mililitr', NULL),
(12, 'łyżeczka', 'Około 5 mililitrów lub 5 gramów'),
(13, 'ziarno', NULL),
(14, 'cm', NULL),
(15, 'plaster', NULL),
(16, 'słoik', NULL),
(17, 'puszka', NULL),
(18, 'listek', NULL),
(19, 'garść', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `preferences`
--

CREATE TABLE `preferences` (
  `user_id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `type` varchar(100) NOT NULL,
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `preparation_time`
--

CREATE TABLE `preparation_time` (
  `id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rate`
--

CREATE TABLE `rate` (
  `recipe_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recipe`
--

CREATE TABLE `recipe` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `portions` int(11) DEFAULT NULL,
  `preparation_time_id` bigint(20) DEFAULT NULL,
  `difficulty_level` varchar(100) DEFAULT NULL,
  `expense` varchar(100) DEFAULT NULL,
  `photo_path` text COLLATE utf8_polish_ci,
  `video_path` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recipe_categories`
--

CREATE TABLE `recipe_categories` (
  `recipe_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recipe_ingredients`
--

CREATE TABLE `recipe_ingredients` (
    `id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `measure_id` bigint(20) NOT NULL,
  `quantity` float NOT NULL,
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recipe_keywords`
--

CREATE TABLE `recipe_keywords` (
  `recipe_id` bigint(20) NOT NULL,
  `keyword_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recipe_recipe_similarity`
--

CREATE TABLE `recipe_recipe_similarity` (
  `recipe1_id` bigint(20) NOT NULL,
  `recipe2_id` bigint(20) NOT NULL,
  `similarity` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `recipe_steps`
--

CREATE TABLE `recipe_steps` (
  `id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `description` text COLLATE utf8_polish_ci NOT NULL,
  `sortable_numer` int(11) NOT NULL,
  `photo_path` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_category_id` (`main_category_id`);

--
-- Indeksy dla tabeli `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `comment_id` (`comment_id`);

--
-- Indeksy dla tabeli `databasechangeloglock`
--
ALTER TABLE `databasechangeloglock`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `jhi_authority`
--
ALTER TABLE `jhi_authority`
  ADD PRIMARY KEY (`name`);

--
-- Indeksy dla tabeli `jhi_persistent_audit_event`
--
ALTER TABLE `jhi_persistent_audit_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `idx_persistent_audit_event` (`principal`,`event_date`);

--
-- Indeksy dla tabeli `jhi_persistent_audit_evt_data`
--
ALTER TABLE `jhi_persistent_audit_evt_data`
  ADD PRIMARY KEY (`event_id`,`name`),
  ADD KEY `idx_persistent_audit_evt_data` (`event_id`);

--
-- Indeksy dla tabeli `jhi_user`
--
ALTER TABLE `jhi_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_user_login` (`login`),
  ADD UNIQUE KEY `ux_user_email` (`email`);

--
-- Indeksy dla tabeli `jhi_user_authority`
--
ALTER TABLE `jhi_user_authority`
  ADD PRIMARY KEY (`user_id`,`authority_name`),
  ADD KEY `fk_authority_name` (`authority_name`);

--
-- Indeksy dla tabeli `keyword`
--
ALTER TABLE `keyword`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `measure`
--
ALTER TABLE `measure`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `preferences`
--
ALTER TABLE `preferences`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- Indeksy dla tabeli `preparation_time`
--
ALTER TABLE `preparation_time`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `rate`
--
ALTER TABLE `rate`
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indeksy dla tabeli `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `preparation_time` (`preparation_time_id`);

--
-- Indeksy dla tabeli `recipe_categories`
--
ALTER TABLE `recipe_categories`
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indeksy dla tabeli `recipe_ingredients`
--
ALTER TABLE `recipe_ingredients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `ingredient_id` (`ingredient_id`),
  ADD KEY `measure_id` (`measure_id`);

--
-- Indeksy dla tabeli `recipe_keywords`
--
ALTER TABLE `recipe_keywords`
  ADD KEY `recipe_id` (`recipe_id`),
  ADD KEY `keyword_id` (`keyword_id`);

--
-- Indeksy dla tabeli `recipe_recipe_similarity`
--
ALTER TABLE `recipe_recipe_similarity`
  ADD KEY `recipe1_id` (`recipe1_id`),
  ADD KEY `recipe2_id` (`recipe2_id`);

--
-- Indeksy dla tabeli `recipe_steps`
--
ALTER TABLE `recipe_steps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipe_id` (`recipe_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `jhi_persistent_audit_event`
--
ALTER TABLE `jhi_persistent_audit_event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `jhi_user`
--
ALTER TABLE `jhi_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`),
  ADD CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`);

--
-- Ograniczenia dla tabeli `jhi_persistent_audit_evt_data`
--
ALTER TABLE `jhi_persistent_audit_evt_data`
  ADD CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`);

--
-- Ograniczenia dla tabeli `jhi_user_authority`
--
ALTER TABLE `jhi_user_authority`
  ADD CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`);

--
-- Ograniczenia dla tabeli `preferences`
--
ALTER TABLE `preferences`
  ADD CONSTRAINT `preferences_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`),
  ADD CONSTRAINT `preferences_ibfk_2` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`);

--
-- Ograniczenia dla tabeli `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`),
  ADD CONSTRAINT `rate_ibfk_2` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`);

--
-- Ograniczenia dla tabeli `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `recipe_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`),
  ADD CONSTRAINT `recipe_ibfk_2` FOREIGN KEY (`preparation_time_id`) REFERENCES `preparation_time` (`id`),
  ADD CONSTRAINT `recipe_ibfk_3` FOREIGN KEY (`preparation_time_id`) REFERENCES `preparation_time` (`id`);

--
-- Ograniczenia dla tabeli `recipe_categories`
--
ALTER TABLE `recipe_categories`
  ADD CONSTRAINT `recipe_categories_ibfk_1` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `recipe_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Ograniczenia dla tabeli `recipe_ingredients`
--
ALTER TABLE `recipe_ingredients`
  ADD CONSTRAINT `recipe_ingredients_ibfk_1` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredient` (`id`),
  ADD CONSTRAINT `recipe_ingredients_ibfk_2` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `recipe_ingredients_ibfk_3` FOREIGN KEY (`measure_id`) REFERENCES `measure` (`id`);

--
-- Ograniczenia dla tabeli `recipe_keywords`
--
ALTER TABLE `recipe_keywords`
  ADD CONSTRAINT `recipe_keywords_ibfk_1` FOREIGN KEY (`keyword_id`) REFERENCES `keyword` (`id`),
  ADD CONSTRAINT `recipe_keywords_ibfk_2` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`);

--
-- Ograniczenia dla tabeli `recipe_recipe_similarity`
--
ALTER TABLE `recipe_recipe_similarity`
  ADD CONSTRAINT `recipe_recipe_similarity_ibfk_1` FOREIGN KEY (`recipe1_id`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `recipe_recipe_similarity_ibfk_2` FOREIGN KEY (`recipe2_id`) REFERENCES `recipe` (`id`);

--
-- Ograniczenia dla tabeli `recipe_steps`
--
ALTER TABLE `recipe_steps`
  ADD CONSTRAINT `recipe_steps_ibfk_1` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
