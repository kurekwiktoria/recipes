package pl.recipes.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.recipes.service.MeasureService;
import pl.recipes.service.dto.MeasureDTO;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MeasureResource {

    private final Logger log = LoggerFactory.getLogger(MeasureResource.class);

    private final MeasureService measureService;

    public MeasureResource(MeasureService measureService) {
        this.measureService = measureService;
    }

    @GetMapping("/measures")
    public ResponseEntity<List<MeasureDTO>> getAllMeasures() {
        final List<MeasureDTO> list = measureService.getAllMeasures();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
