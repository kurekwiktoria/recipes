package pl.recipes.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.recipes.domain.IngredientEntity;
import pl.recipes.service.IngredientService;
import pl.recipes.service.dto.IngredientDTO;
import pl.recipes.web.rest.errors.BadRequestAlertException;
import pl.recipes.web.rest.util.HeaderUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class IngredientResource {

    private final Logger log = LoggerFactory.getLogger(IngredientResource.class);

    private final IngredientService ingredientService;

    public IngredientResource(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @GetMapping("/ingredients")
    public ResponseEntity<List<IngredientDTO>> getAllIngredients() {
        final List<IngredientDTO> list = ingredientService.getAllIngredients();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/ingredients")
    public ResponseEntity<IngredientEntity> createIngredient(@Valid @RequestBody IngredientDTO ingredientDTO) throws URISyntaxException {
        if (ingredientDTO.getId() != null) {
            throw new BadRequestAlertException("A new ingredient cannot already have an ID", "userManagement", "idexists");
            // Lowercase the user login before comparing with database
        } else {
            IngredientEntity ingredientEntity = ingredientService.createIngredient(ingredientDTO);
            return ResponseEntity.created(new URI("/api/ingredients/" + ingredientEntity.getId()))
                .headers(HeaderUtil.createAlert("ingredient.add.created", ingredientEntity.getId().toString()))
                .body(ingredientEntity);
        }
    }

}
