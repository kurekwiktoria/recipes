package pl.recipes.web.rest;

import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.recipes.domain.CommentEntity;
import pl.recipes.domain.RecipeEntity;
import pl.recipes.repository.CommentRepository;
import pl.recipes.service.RecipeService;
import pl.recipes.service.RecommendationService;
import pl.recipes.service.dto.CommentDTO;
import pl.recipes.service.dto.RateDTO;
import pl.recipes.service.dto.RecipeDTO;
import pl.recipes.web.rest.errors.BadRequestAlertException;
import pl.recipes.web.rest.errors.CommentNotFoundException;
import pl.recipes.web.rest.util.HeaderUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RecipeResource {

    private final Logger log = LoggerFactory.getLogger(RecipeResource.class);

    private final RecipeService recipeService;
    private final CommentRepository commentRepository;
    private final RecommendationService recommendationService;

    public RecipeResource(RecipeService recipeService, CommentRepository commentRepository,
                          RecommendationService recommendationService) {
        this.recipeService = recipeService;
        this.commentRepository = commentRepository;
        this.recommendationService = recommendationService;
    }

    @PostMapping("/recipes")
    public ResponseEntity<RecipeEntity> createRecipe(@Valid @RequestBody RecipeDTO recipeDTO) throws URISyntaxException {
        if (recipeDTO.getId() != null) {
            throw new BadRequestAlertException("A new recipe cannot already have an ID", "userManagement", "idexists");
            // Lowercase the user login before comparing with database
        } else {
            RecipeEntity newRecipe = recipeService.createRecipe(recipeDTO);
            return ResponseEntity.created(new URI("/api/recipes/" + newRecipe.getId()))
                .headers(HeaderUtil.createAlert("recipes.add.created", newRecipe.getId().toString()))
                .body(newRecipe);
        }
    }

    @GetMapping("/recipes")
    public ResponseEntity<List<RecipeDTO>> getAllRecipes() {
        final List<RecipeDTO> list = recipeService.getAllRecipes();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/recipes/details/{id}")
    public ResponseEntity<RecipeDTO> getRecipe(@PathVariable Long id) {
        return new ResponseEntity<>(recipeService.getRecipe(id), HttpStatus.OK);
    }

    @GetMapping("/recipes/{category}")
    public ResponseEntity<List<RecipeDTO>> getRecipesByCategory(@PathVariable String category) {
        log.debug("REST request to get Recipe : {}");
        final List<RecipeDTO> list = recipeService.getAllRecipesByCategory(category);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/newRecipes")
    public ResponseEntity<List<RecipeDTO>> getNewRecipes() {
        final List<RecipeDTO> list = recipeService.getNewRecipes();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/recipes/rate")
    public ResponseEntity<Integer> rateRecipe(@Valid @RequestBody RateDTO rateDTO) throws URISyntaxException {
        Integer rateValue = recipeService.rateRecipe(rateDTO);
        return ResponseEntity.created(new URI("/api/recipes/" + rateValue))
            .headers(HeaderUtil.createAlert("recipes.rate", rateValue.toString()))
            .body(rateValue);
    }

    @GetMapping("/recipes/search/{params}")
    public ResponseEntity<List<RecipeDTO>> searchRecipes(@PathVariable String params) {
        log.debug("REST request to get Recipes : {}");
        final List<RecipeDTO> list = recipeService.searchRecipes(params);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/recipes/comment")
    public ResponseEntity<CommentEntity> commentRecipe(@Valid @RequestBody CommentDTO commentDTO) throws URISyntaxException {
        CommentEntity comment = recipeService.commentRecipe(commentDTO);
        return ResponseEntity.created(new URI("/api/recipes/" + comment.getId()))
            .headers(HeaderUtil.createAlert("recipes.comment.add", comment.getId().toString()))
            .body(comment);
    }

    @PutMapping("/recipes/comment")
    public ResponseEntity<CommentDTO> removeComment(@Valid @RequestBody CommentDTO commentDTO) {
        log.debug("REST request to update Comment : {}", commentDTO);
        Optional<CommentEntity> existingComment = commentRepository.findById(commentDTO.getId());
        if (!existingComment.isPresent()) {
            throw new CommentNotFoundException();
        }

        Optional<CommentDTO> updatedComment = recipeService.updateComment(commentDTO);

        return ResponseUtil.wrapOrNotFound(updatedComment,
            HeaderUtil.createAlert("recipe.comment.remove", commentDTO.getId().toString()));
    }

    @DeleteMapping("/recipes/{recipeId}")
    public ResponseEntity<Void> deleteRecipe(@PathVariable Long recipeId) {
        log.debug("REST request to delete Recipe: {}", recipeId);
        recipeService.deleteRecipe(recipeId);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "recipes.deleted", recipeId.toString())).build();
    }

    @GetMapping("/recommendations/{userLogin}")
    public ResponseEntity<List<RecipeDTO>> getRecommendations(@PathVariable String userLogin) {
        log.debug("REST request to get Recommendations : {}");
        List<RecipeDTO> list = recommendationService.getRecommendationsForUser(userLogin);
        if(list.isEmpty())
            list = recipeService.getNewRecipes();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
