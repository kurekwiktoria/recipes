/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.recipes.web.rest.vm;
