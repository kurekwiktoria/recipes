package pl.recipes.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.recipes.domain.FavouriteEntity;
import pl.recipes.service.FavouritesService;
import pl.recipes.service.dto.FavouriteDTO;
import pl.recipes.web.rest.errors.BadRequestAlertException;
import pl.recipes.web.rest.util.HeaderUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class FavouritesResource {

    private final Logger log = LoggerFactory.getLogger(FavouritesResource.class);

    private final FavouritesService favouriteService;

    public FavouritesResource(FavouritesService favouriteService) {
        this.favouriteService = favouriteService;
    }

    @PostMapping("/favourites")
    public ResponseEntity<FavouriteEntity> createFavourite(@Valid @RequestBody FavouriteDTO favouriteDTO) throws URISyntaxException {
        if (favouriteDTO.getId() != null) {
            throw new BadRequestAlertException("A new favourite cannot already have an ID", "userManagement", "idexists");
            // Lowercase the user login before comparing with database
        } else {
            FavouriteEntity newFavourite = favouriteService.createFavouriteList(favouriteDTO);
            return ResponseEntity.created(new URI("/api/favourites/" + newFavourite.getId()))
                .headers(HeaderUtil.createAlert("favourites.add.created", favouriteDTO.getName()))
                .body(newFavourite);
        }
    }

    @GetMapping("/favourites/details/{id}")
    public ResponseEntity<FavouriteDTO> getFavourite(@PathVariable Long id) {
        return new ResponseEntity<>(favouriteService.getFavourite(id), HttpStatus.OK);
    }

    @GetMapping("/favourites/{userLogin}")
    public ResponseEntity<List<FavouriteDTO>> findByUserLogin(@PathVariable String userLogin) {
        log.debug("REST request to get Favourite : {}");
        final List<FavouriteDTO> list = favouriteService.getAllForUser(userLogin);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/find/{userLogin}/{recipeId}")
    public ResponseEntity<List<FavouriteDTO>> findByUserLoginAndNotRecipeId(@PathVariable String userLogin, @PathVariable Long recipeId) {
        log.debug("REST request to get Favourite : {}");
        final List<FavouriteDTO> list = favouriteService.getAllForUserWithoutRecipe(userLogin, recipeId);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @DeleteMapping("/favourites/{id}")
    public ResponseEntity<Void> deleteFavourite(@PathVariable Long id) {
        log.debug("REST request to delete Favourite: {}", id);
        favouriteService.deleteFavourite(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "favourites.deleted.list", id.toString())).build();
    }

    @DeleteMapping("/favourites/{recipeId}/{favouriteId}")
    public ResponseEntity<Void> deleteRecipeFromFavourite(@PathVariable Long recipeId, @PathVariable Long favouriteId) {
        log.debug("REST request to delete Recipe from Favourite: {}", recipeId);
        favouriteService.deleteRecipeFromFavourite(recipeId, favouriteId);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "favourites.deleted.recipe", recipeId.toString())).build();
    }

    @GetMapping("/favourites/{recipeId}/{favouriteId}")
    public ResponseEntity<FavouriteDTO> addRecipeToFavourite(@PathVariable Long recipeId, @PathVariable Long favouriteId) {
        log.debug("REST request to add Recipe to Favourite: {}", recipeId);
        favouriteService.addRecipeToFavourite(recipeId, favouriteId);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "favourites.add.recipe", recipeId.toString())).build();
    }

    @GetMapping("/favourites/check/{userLogin}/{recipeId}")
    public ResponseEntity<Boolean> checkUserLikedRecipe(@PathVariable String userLogin, @PathVariable Long recipeId) {
        return new ResponseEntity<>(favouriteService.checkUserLikedRecipe(userLogin, recipeId), HttpStatus.OK);
    }
}
