package pl.recipes.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.recipes.service.FileService;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class FileResource {

    private final Logger log = LoggerFactory.getLogger(FileResource.class);

    private final FileService fileService;

    public FileResource(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/saveFile")
    public ResponseEntity<String> saveFile(MultipartFile file) {
        return ResponseEntity.ok(fileService.save(file));
    }

    @GetMapping("/getFile")
    public ResponseEntity<Resource> getFile(String path){
        return ResponseEntity.ok(fileService.getFile(path));
    }

    @GetMapping(value = "/downloadFile")
    public ResponseEntity<Resource> downloadFile(@RequestParam String path) {
        log.debug("Pobieranie pliku ze ścieżki = " + path);
        Resource file = fileService.getFile(path);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.setContentType(MediaType.IMAGE_JPEG);
//        headers.setContentDisposition(ContentDisposition.builder("formData").filename(file.getFilename()).build());
//        headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
        long length = 0;
        try {
            length = file.contentLength();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
            .headers(headers)
            .contentLength(length)
            .body(file);
    }
}
