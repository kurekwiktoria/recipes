package pl.recipes.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class CommentNotFoundException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    public CommentNotFoundException() {
        super(ErrorConstants.DEFAULT_TYPE, "Comment not found", Status.BAD_REQUEST);
    }
}
