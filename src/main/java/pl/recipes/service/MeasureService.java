package pl.recipes.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.repository.MeasureRepository;
import pl.recipes.service.dto.MeasureDTO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing measures.
 */
@Service
@Transactional
public class MeasureService {

    private final Logger log = LoggerFactory.getLogger(MeasureService.class);

    private final MeasureRepository measureRepository;

    public MeasureService(MeasureRepository measureRepository) {
        this.measureRepository = measureRepository;
    }

    @Transactional(readOnly = true)
    public List<MeasureDTO> getAllMeasures() {
        return measureRepository.findAll().stream().map(MeasureDTO::new).collect(Collectors.toList());
    }

}
