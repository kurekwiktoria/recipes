package pl.recipes.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.domain.*;
import pl.recipes.repository.*;
import pl.recipes.service.dto.*;
import pl.recipes.service.util.CommentTypeEnum;
import pl.recipes.service.util.DifficultyLevelEnum;
import pl.recipes.service.util.ExpenseEnum;
import pl.recipes.service.util.PreparationTimeEnum;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for managing recipes.
 */
@Service
@Transactional
public class RecipeService {

    private final Logger log = LoggerFactory.getLogger(RecipeService.class);

    private final RecipeRepository recipeRepository;
    private final CategoryRepository categoryRepository;
    private final CommentRepository commentRepository;
    private final RateRepository rateRepository;
    private final KeywordRepository keywordRepository;
    private final IngredientRepository ingredientRepository;
    private final PreparationTimeRepository preparationTimeRepository;
    private final MeasureRepository measureRepository;
    private final RecipeIngredientRepository recipeIngredientRepository;
    private final RecipeStepRepository recipeStepRepository;
    private final UserRepository userRepository;
    private final CategoryService categoryService;
    private final RecommendationRepository recommendationRepository;

    public RecipeService(RecipeRepository recipeRepository, CategoryService categoryService,
                         CategoryRepository categoryRepository, CommentRepository commentRepository,
                         KeywordRepository keywordRepository, PreparationTimeRepository preparationTimeRepository,
                         IngredientRepository ingredientRepository, MeasureRepository measureRepository,
                         RecipeIngredientRepository recipeIngredientRepository, RecipeStepRepository recipeStepRepository,
                         UserRepository userRepository, RateRepository rateRepository,
                         RecommendationRepository recommendationRepository) {
        this.recipeRepository = recipeRepository;
        this.categoryService = categoryService;
        this.categoryRepository = categoryRepository;
        this.commentRepository = commentRepository;
        this.keywordRepository = keywordRepository;
        this.preparationTimeRepository = preparationTimeRepository;
        this.ingredientRepository = ingredientRepository;
        this.measureRepository = measureRepository;
        this.recipeIngredientRepository = recipeIngredientRepository;
        this.recipeStepRepository = recipeStepRepository;
        this.userRepository = userRepository;
        this.rateRepository = rateRepository;
        this.recommendationRepository = recommendationRepository;
    }

    @Transactional
    public RecipeEntity createRecipe(RecipeDTO recipeDTO) {
        RecipeEntity recipe = new RecipeEntity();
        recipe.setActive(true);
        if (recipeDTO.getCategories() != null) {
            List<CategoryEntity> categories = recipeDTO.getCategories().stream()
                .map((CategoryDTO dto) -> categoryRepository.findById(dto.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
            recipe.setCategories(categories);
        }
        recipe.setDifficultyLevel(DifficultyLevelEnum.valueOf(recipeDTO.getDifficultyLevel()));
        recipe.setExpense(ExpenseEnum.valueOf(recipeDTO.getExpense()));
        if (recipeDTO.getKeywords() != null) {
            List<KeywordEntity> keywords = recipeDTO.getKeywords().stream()
                .map(this::createKeywordFromDto)
                .collect(Collectors.toList());
            recipe.setKeywords(keywords);
        }

        recipe.setPhotoPath(recipeDTO.getPhotoPath());
        recipe.setVideoPath(recipeDTO.getVideoPath());
        recipe.setPortions(recipeDTO.getPortions());

        if (recipeDTO.getPreparationTime() != null) {

            PreparationTimeEntity preparationTimeEntity;
            PreparationTimeDTO dto = recipeDTO.getPreparationTime();

            preparationTimeEntity = preparationTimeRepository.findByQuantityEqualsAndTypeEquals(dto.getQuantity(), PreparationTimeEnum.MINUTES)
                .orElseGet(() -> {
                    PreparationTimeEntity entity = new PreparationTimeEntity();
                    entity.setQuantity(dto.getQuantity());
                    entity.setType(PreparationTimeEnum.MINUTES);
                    preparationTimeRepository.save(entity);
                    return entity;
                });
            recipe.setPreparationTime(preparationTimeEntity);
        }

        recipe.setTitle(recipeDTO.getTitle());
        userRepository.findOneByLogin(recipeDTO.getUserLogin()).ifPresent(recipe::setUserEntity);
        recipeRepository.save(recipe);

        if (recipeDTO.getRecipeIngredients() != null) {
            List<RecipeIngredientEntity> recipeIngredientEntities = recipeDTO.getRecipeIngredients().stream()
                .map((RecipeIngredientDTO dto) -> createRecipeIngredientsFromDto(dto, recipe))
                .collect(Collectors.toList());
        }

        if (recipeDTO.getSteps() != null) {
            List<RecipeStepEntity> steps = recipeDTO.getSteps().stream()
                .map((RecipeStepDTO dto) -> createStepsFromDto(dto, recipe))
                .collect(Collectors.toList());
        }

        recipeRepository.save(recipe);
        return recipe;
    }

    @Transactional(readOnly = true)
    public List<RecipeDTO> getAllRecipes() {
        return recipeRepository.findAll().stream().map(RecipeDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public RecipeDTO getRecipe(Long id) {
        Optional<RecipeEntity> recipeEntity = recipeRepository.findById(id);
        return recipeEntity.map(RecipeDTO::new).orElseGet(RecipeDTO::new);
    }

    @Transactional(readOnly = true)
    public List<RecipeDTO> getNewRecipes() {
        return recipeRepository.findTop20ByPhotoPathIsNotNullAndActiveTrueOrderByIdDesc().stream().map(RecipeDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<RecipeDTO> getAllRecipesByCategory(String name) {
        switch (name) {
            case "sniadania":
                name = "śniadania";
                break;
            case "salatki":
                name = "sałatki";
                break;
            case "dania-glowne":
                name = "dania główne";
                break;
            case "surowki":
                name = "surówki";
                break;
            default:
                break;
        }
        CategoryDTO mainCategory = categoryService.getCategory(name);
        List<Long> subCategories = categoryService.getCategoriesByMainCategoryId(mainCategory.getId());
        return recipeRepository.findAllByCategoriesIdInAndActiveTrue(subCategories).stream().map(RecipeDTO::new).collect(Collectors.toList());
    }

    private KeywordEntity createKeywordFromDto(KeywordDTO dto) {
        return keywordRepository.findByNameEquals(dto.getName()).orElseGet(() -> {
            KeywordEntity keywordEntity = new KeywordEntity();
            keywordEntity.setName(dto.getName());
            keywordRepository.save(keywordEntity);
            return keywordEntity;

        });
    }

    private RecipeIngredientEntity createRecipeIngredientsFromDto(RecipeIngredientDTO dto, RecipeEntity recipeEntity) {
        IngredientEntity ingredientEntity = ingredientRepository.getOne(dto.getIngredient().getId());
        RecipeIngredientEntity recipeIngredientEntity = new RecipeIngredientEntity();
        recipeIngredientEntity.setDescription(dto.getDescription());
        recipeIngredientEntity.setQuantity(dto.getQuantity());
        recipeIngredientEntity.setIngredient(ingredientEntity);
        recipeIngredientEntity.setRecipe(recipeEntity);
        MeasureEntity measureEntity = measureRepository.getOne(dto.getMeasure().getId());
        recipeIngredientEntity.setMeasure(measureEntity);

        recipeIngredientRepository.save(recipeIngredientEntity);

        return recipeIngredientEntity;
    }

    private RecipeStepEntity createStepsFromDto(RecipeStepDTO dto, RecipeEntity recipeEntity) {
        RecipeStepEntity recipeStepEntity = new RecipeStepEntity();
        recipeStepEntity.setDescription(dto.getDescription());
        recipeStepEntity.setPhoto(dto.getPhoto());
        recipeStepEntity.setSortableNumber(1);
        recipeStepEntity.setRecipe(recipeEntity);

        recipeStepRepository.save(recipeStepEntity);
        return recipeStepEntity;
    }

    @Transactional
    public Integer rateRecipe(RateDTO rateDTO) {
        return rateRepository.findByRecipeIdEqualsAndUserLoginEquals(rateDTO.getRecipeId(), rateDTO.getUserLogin())
            .map(rateEntity -> {
                rateEntity.setValue(rateDTO.getValue());
                return getRecipeRate(rateDTO.getRecipeId());
            })
            .orElseGet(() -> {
                RateEntity rate = new RateEntity();
                rate.setRecipe(recipeRepository.getOne(rateDTO.getRecipeId()));
                userRepository.findOneByLogin(rateDTO.getUserLogin()).ifPresent(rate::setUser);
                rate.setValue(rateDTO.getValue());
                rateRepository.save(rate);
                rateRepository.flush();
                return getRecipeRate(rateDTO.getRecipeId());
            });
    }

    @Transactional
    public CommentEntity commentRecipe(CommentDTO dto) {
        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setDescription(dto.getDescription());
        commentEntity.setType(CommentTypeEnum.valueOf(dto.getType()));
        commentEntity.setRecipe(recipeRepository.getOne(dto.getRecipeId()));
        if(dto.getCommentId() != null) commentRepository.findById(dto.getCommentId()).ifPresent(commentEntity::setComment);
        userRepository.findOneByLogin(dto.getUserLogin()).ifPresent(commentEntity::setUser);

        commentRepository.save(commentEntity);
        return commentEntity;
    }

    @Transactional(readOnly = false)
    public void deleteRecipe(Long recipeId) {
        RecipeEntity recipeEntity = recipeRepository.getOne(recipeId);
        recipeEntity.setActive(false);
        recipeRepository.save(recipeEntity);
    }

    @Transactional
    public Optional<CommentDTO> updateComment(CommentDTO dto) {
        return Optional.of(commentRepository
            .findById(dto.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(comment -> {
                comment.setDescription("Komentarz został usunięty przez autora.");
                return comment;
            })
            .map(CommentDTO::new);
    }

    private Integer getRecipeRate(Long recipeId){
        RecipeEntity recipeEntity = recipeRepository.getOne(recipeId);
        if(!recipeEntity.getRates().isEmpty()){
            IntSummaryStatistics stats = recipeEntity.getRates().stream()
                .mapToInt(RateEntity::getValue)
                .summaryStatistics();
            return Math.toIntExact(Math.round(stats.getAverage()));
        }
        return 0;
    }

    public List<RecipeDTO> searchRecipes(String params){
        return recipeRepository.searchRecipes(params).stream().map(RecipeDTO::new).collect(Collectors.toList());
    }
}
