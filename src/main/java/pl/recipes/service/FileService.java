package pl.recipes.service;

import org.apache.logging.log4j.LoggingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.jcr.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

/**
 * Service class for managing categories.
 */
@Service
@Transactional
public class FileService {

    private Repository jcrRepository;
    private final Logger log = LoggerFactory.getLogger(FileService.class);


    @Autowired
    public FileService(Repository jcrRepository) throws Exception {
        this.jcrRepository = jcrRepository;
        Session session = getSession();
        try {
            Calendar today = Calendar.getInstance();
            today.setTime(new Date());
            String year = today.get(Calendar.YEAR) + "";
            String month = today.get(Calendar.MONTH) + "";
            String day = today.get(Calendar.DAY_OF_MONTH)+ "";

            Node rootNode = session.getRootNode();
            Node scans;
            if ( !rootNode.hasNode("scans"))
                scans = rootNode.addNode("scans", "nt:folder");
            else
                scans = rootNode.getNode("scans");

            Node yearNode;
            if ( !scans.hasNode( year ))
                yearNode = scans.addNode(year, "nt:folder");
            else
                yearNode = scans.getNode(year);

            Node monthNode;
            if ( !yearNode.hasNode(month))
                monthNode = yearNode.addNode(month, "nt:folder");
            else
                monthNode = yearNode.getNode(month);

            if ( !monthNode.hasNode(day))
                monthNode.addNode(day, "nt:folder");

            session.save();
        } finally {
            session.logout();
        }
    }

    private Session getSession() {
        Session session = null;

        try {
            session = jcrRepository.login(new SimpleCredentials("admin", "admin".toCharArray()));
        }
        catch (LoggingException | RepositoryException e ) {
            log.info(e.getMessage());
        }
        return session;
    }

    public Resource getFile(String path) {
        Session session = getSession();

        try {
            Node root = session.getRootNode();
            // usuwamy pierwszy /, żeby ścieżka była względna
            path = path.substring(1);
            Node target = root.getNode(path);

            Binary binary = target.getNode("jcr:content").getProperty("jcr:data").getBinary();
            String fileName = target.getName();
            target.remove();
            byte [] data = new byte[(int) binary.getSize()];

            binary.getStream().read(data);

            ByteArrayResource resource = new ByteArrayResource(data) {
                @Override
                public String getFilename() {
                    return fileName;
                }
            };

//            System.out.println(resource.getFilename());
            return resource;
        }
        catch ( Exception e) {
            log.info(e.getMessage());
        }
        finally {
            session.logout();
        }

        return null;
    }

    public byte[] getFileAsBytes(String path) {
        Session session = getSession();

        try {
            Node root = session.getRootNode();
            // usuwamy pierwszy /, żeby ścieżka była względna
            path = path.substring(1);
            Node target = root.getNode(path);

            Binary binary = target.getNode("jcr:content").getProperty("jcr:data").getBinary();
            byte [] data = new byte[(int) binary.getSize()];
            binary.getStream().read(data);

            return data;
        }
        catch (Exception e) {
            log.info(e.getMessage());
        }

        return null;
    }

    public String save(MultipartFile file) {
        Session session = getSession();
        String filePath = "";
        InputStream stream = null;
        try {
            stream = file.getInputStream();
        } catch (IOException e ) {
            System.out.println(e.getMessage());
        }

        try {
            Node root = session.getRootNode();

            Calendar today = Calendar.getInstance();
            today.setTime(new Date());
            String year = today.get(Calendar.YEAR) + "";
            String month = today.get(Calendar.MONTH) + "";
            String day = today.get(Calendar.DAY_OF_MONTH)+ "";

            Node dayNode = root.getNode("scans/" + year + "/" + month + "/" + day);
            Node fileFolder = dayNode.addNode(dayNode.getNodes().getSize() + 1 + "", "nt:folder");
            Node fileNode = fileFolder.addNode(file.getOriginalFilename(), "nt:file");
            Node content = fileNode.addNode("jcr:content", "nt:resource");

            Binary binary = session.getValueFactory().createBinary(stream);
            content.setProperty("jcr:data", binary);
            content.setProperty("jcr:mimeType", file.getContentType());
            content.setProperty("jcr:lastModified", today);

            System.out.println("Scieżka pliku: " + fileNode.getPath());

            session.save();
            filePath = fileNode.getPath();
        } catch (Exception e ) {
            log.info(e.getMessage());
        } finally {
            session.logout();
        }

        return filePath;
    }

    public void delete(String path) {
        Session session = getSession();

        try {
            Node root = session.getRootNode();
            // usuwamy pierwszy /, żeby ścieżka była względna
            path = path.substring(1);
            Node target = root.getNode(path);
            target.remove();
        }
        catch ( Exception e) {
            log.info(e.getMessage());
        }
        finally {
            session.logout();
        }
    }

}
