package pl.recipes.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.domain.FavouriteEntity;
import pl.recipes.domain.RecipeEntity;
import pl.recipes.repository.FavouriteRepository;
import pl.recipes.repository.RecipeRepository;
import pl.recipes.repository.UserRepository;
import pl.recipes.service.dto.FavouriteDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for managing favourites.
 */
@Service
@Transactional
public class FavouritesService {

    private final Logger log = LoggerFactory.getLogger(FavouritesService.class);

    private final FavouriteRepository favouriteRepository;
    private final UserRepository userRepository;
    private final RecipeRepository recipeRepository;

    public FavouritesService(FavouriteRepository favouriteRepository, UserRepository userRepository,
                             RecipeRepository recipeRepository) {
        this.favouriteRepository = favouriteRepository;
        this.userRepository = userRepository;
        this.recipeRepository = recipeRepository;
    }

    @Transactional
    public FavouriteEntity createFavouriteList(FavouriteDTO favouriteDTO) {
        FavouriteEntity favouriteEntity = new FavouriteEntity();
        favouriteEntity.setName(favouriteDTO.getName());
        favouriteEntity.setDescription(favouriteDTO.getDescription());
        userRepository.findOneByLogin(favouriteDTO.getUserLogin()).ifPresent(favouriteEntity::setUser);
        favouriteRepository.save(favouriteEntity);
        return favouriteEntity;
    }

    @Transactional(readOnly = true)
    public List<FavouriteDTO> getAllForUser(String userLogin) {
        return favouriteRepository.findByUserLoginEquals(userLogin).stream().map(FavouriteDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<FavouriteDTO> getAllForUserWithoutRecipe(String userLogin, Long recipeId) {
        RecipeEntity recipeEntity = recipeRepository.getOne(recipeId);
        List<FavouriteEntity> favs = favouriteRepository.findByUserLoginEquals(userLogin);
        List<FavouriteEntity> notAddedFavs = new ArrayList<>();
        for(FavouriteEntity fav : favs) {
            if(!fav.getRecipes().contains(recipeEntity))
                notAddedFavs.add(fav);
        }
        return notAddedFavs.stream().map(FavouriteDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public boolean checkUserLikedRecipe(String userLogin, Long recipeId) {
        RecipeEntity recipeEntity = recipeRepository.getOne(recipeId);
        List<FavouriteEntity> favs = favouriteRepository.findByUserLoginEquals(userLogin);
        for(FavouriteEntity fav : favs) {
            if(fav.getRecipes().contains(recipeEntity))
                return true;
        }
        return false;
    }

    @Transactional(readOnly = true)
    public FavouriteDTO getFavourite(Long id) {
        Optional<FavouriteEntity> favouriteEntity = favouriteRepository.findById(id);
        return favouriteEntity.map(FavouriteDTO::new).orElseGet(FavouriteDTO::new);
    }

    @Transactional(readOnly = false)
    public void deleteFavourite(Long id) {
        FavouriteEntity favouriteEntity = favouriteRepository.getOne(id);
        favouriteEntity.getRecipes().forEach(recipe -> {
            recipe.getFavourites().remove(favouriteEntity);
        });
        favouriteRepository.delete(favouriteEntity);
        favouriteRepository.flush();
    }

    @Transactional(readOnly = false)
    public void deleteRecipeFromFavourite(Long recipeId, Long favouriteId) {
        FavouriteEntity favouriteEntity = favouriteRepository.getOne(favouriteId);
        RecipeEntity recipe = recipeRepository.getOne(recipeId);
        recipe.getFavourites().remove(favouriteEntity);
        favouriteRepository.flush();
    }

    @Transactional(readOnly = false)
    public void addRecipeToFavourite(Long recipeId, Long favouriteId) {
        FavouriteEntity favouriteEntity = favouriteRepository.getOne(favouriteId);
        RecipeEntity recipe = recipeRepository.getOne(recipeId);
        recipe.getFavourites().add(favouriteEntity);
        favouriteRepository.flush();
    }
}
