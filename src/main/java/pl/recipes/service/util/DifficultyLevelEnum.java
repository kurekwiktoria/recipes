package pl.recipes.service.util;


public enum DifficultyLevelEnum implements Described {
    EASY("Łatwe"),
    MEDIUM("Średnie"),
    DIFFICULT("Trudne");

    private String description;

    DifficultyLevelEnum(String description){
        this.description = description;
    }

    @Override
    public String getHumanReadableDescription(){
        return description;
    }
}
