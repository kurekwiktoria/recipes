package pl.recipes.service.util;


public enum ExpenseEnum implements Described {
    CHEAP("Tanie"),
    MEDIUM("Niedrogie"),
    EXPENSIVE("Drogie");

    private String description;

    ExpenseEnum(String description){
        this.description = description;
    }

    @Override
    public String getHumanReadableDescription(){
        return description;
    }
}
