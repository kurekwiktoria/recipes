package pl.recipes.service.util;


public enum PreparationTimeEnum implements Described {
    DAYS("dni"),
    HOURS("godziny"),
    MINUTES("minuty");

    private String description;

    PreparationTimeEnum(String description){
        this.description = description;
    }

    @Override
    public String getHumanReadableDescription(){
        return description;
    }
}
