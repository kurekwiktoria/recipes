package pl.recipes.service.util;

/**
 * Interfejs dla enumów z opisem
 */
public interface Described {

    String getHumanReadableDescription();
}
