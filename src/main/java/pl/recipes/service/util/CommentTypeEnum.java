package pl.recipes.service.util;


public enum CommentTypeEnum implements Described {
    PRIVATE("Prywatny"),
    PUBLIC("Publiczny"),
    QUESTION("Pytanie"),
    OPINION("Opinia"),
    ADVICE("Porada");

    private String description;

    CommentTypeEnum(String description){
        this.description = description;
    }

    @Override
    public String getHumanReadableDescription(){
        return description;
    }
}
