package pl.recipes.service.util;


public enum PreferencesTypeEnum implements Described {
    LIKED("Lubię"),
    DISLIKED("Nie lubię");

    private String description;

    PreferencesTypeEnum(String description){
        this.description = description;
    }

    @Override
    public String getHumanReadableDescription(){
        return description;
    }
}
