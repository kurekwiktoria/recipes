package pl.recipes.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.domain.IngredientEntity;
import pl.recipes.repository.IngredientRepository;
import pl.recipes.service.dto.IngredientDTO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing measures.
 */
@Service
@Transactional
public class IngredientService {

    private final Logger log = LoggerFactory.getLogger(IngredientService.class);

    private final IngredientRepository ingredientRepository;

    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @Transactional(readOnly = true)
    public List<IngredientDTO> getAllIngredients() {
        return ingredientRepository.findAll().stream().map(IngredientDTO::new).collect(Collectors.toList());
    }

    @Transactional
    public IngredientEntity createIngredient(IngredientDTO ingredientDTO) {
        IngredientEntity ingredientEntity = new IngredientEntity();
        ingredientEntity.setName(ingredientDTO.getName());
        ingredientEntity.setDescription(ingredientDTO.getDescription());

        ingredientRepository.save(ingredientEntity);

        return ingredientEntity;
    }

}
