package pl.recipes.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.repository.RecommendationRepository;
import pl.recipes.service.dto.RecipeDTO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing recommendation.
 */
@Service
@Transactional
public class RecommendationService {

    private final Logger log = LoggerFactory.getLogger(RecommendationService.class);

    private final RecommendationRepository recommendationRepository;

    public RecommendationService(RecommendationRepository recommendationRepository) {
        this.recommendationRepository = recommendationRepository;
    }

   public List<RecipeDTO> getRecommendationsForUser(String userLogin) {
        return this.recommendationRepository.getRecommendations(userLogin).stream().map(RecipeDTO::new).collect(Collectors.toList());
   }
}
