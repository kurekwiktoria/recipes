package pl.recipes.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.repository.CategoryRepository;
import pl.recipes.service.dto.CategoryDTO;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class for managing categories.
 */
@Service
@Transactional
public class CategoryService {

    private final Logger log = LoggerFactory.getLogger(CategoryService.class);

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Transactional(readOnly = true)
    public List<CategoryDTO> getAllCategories() {
        return categoryRepository.findAllByOrderByName().stream().map(CategoryDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public CategoryDTO getCategory(String name) {
        return new CategoryDTO(categoryRepository.findByNameEqualsIgnoreCase(name));
    }

    @Transactional(readOnly = true)
    public List<Long> getCategoriesByMainCategoryId(Long mainCategoryId){
        return categoryRepository.getAllIdsByMainCategoryId(mainCategoryId);
    }
}
