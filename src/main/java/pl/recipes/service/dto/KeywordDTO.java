package pl.recipes.service.dto;

import pl.recipes.domain.KeywordEntity;

public class KeywordDTO {

    private Long id;
    private String name;

    public KeywordDTO() {
        // Empty constructor needed for Jackson.
    }

    public KeywordDTO(KeywordEntity keywordEntity) {
        this.id = keywordEntity.getId();
        this.name = keywordEntity.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "KeywordDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }
}
