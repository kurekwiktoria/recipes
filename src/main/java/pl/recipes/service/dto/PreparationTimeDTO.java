package pl.recipes.service.dto;

import pl.recipes.domain.PreparationTimeEntity;

public class PreparationTimeDTO {

    private Long id;
    private int quantity;
    private String type;


    public PreparationTimeDTO() {
        // Empty constructor needed for Jackson.
    }

    public PreparationTimeDTO(PreparationTimeEntity preparationTimeEntity) {
        this.id = preparationTimeEntity.getId();
        this.quantity = preparationTimeEntity.getQuantity();
        this.type = preparationTimeEntity.getType().name();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PreparationTimeDTO{" +
            "id=" + id +
            ", quantity=" + quantity +
            ", type='" + type + '\'' +
            '}';
    }
}
