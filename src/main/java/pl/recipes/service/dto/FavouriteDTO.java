package pl.recipes.service.dto;

import pl.recipes.domain.FavouriteEntity;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FavouriteDTO {

    private Long id;
    private String name;
    private String description;
    private String userLogin;
    private List<RecipeDTO> recipes;

    public FavouriteDTO() {
        // Empty constructor needed for Jackson.
    }

    public FavouriteDTO(FavouriteEntity favouriteEntity) {
        this.id = favouriteEntity.getId();
        this.name = favouriteEntity.getName();
        this.userLogin = favouriteEntity.getUser().getLogin();
        this.description = favouriteEntity.getDescription();
        this.recipes = favouriteEntity.getRecipes().isEmpty() ?
            Collections.emptyList() :
            favouriteEntity.getRecipes().stream().map(RecipeDTO::new).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public List<RecipeDTO> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeDTO> recipes) {
        this.recipes = recipes;
    }

    @Override
    public String toString() {
        return "FavouriteDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", userLogin='" + userLogin + '\'' +
            '}';
    }
}
