package pl.recipes.service.dto;

import pl.recipes.domain.PreferenceEntity;

public class PreferenceDTO {

    private Long userId;
    private Long recipeId;
    private String type;
    private String description;


    public PreferenceDTO() {
        // Empty constructor needed for Jackson.
    }

    public PreferenceDTO(PreferenceEntity preferenceEntity) {
        this.userId = preferenceEntity.getId().getUserId();
        this.recipeId = preferenceEntity.getId().getRecipeId();
        this.type = preferenceEntity.getType().name();
        this.description = preferenceEntity.getDescription();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PreferenceDTO{" +
            "userId=" + userId +
            ", recipeId=" + recipeId +
            ", type='" + type + '\'' +
            ", description='" + description + '\'' +
            '}';
    }
}
