package pl.recipes.service.dto;

import pl.recipes.domain.IngredientEntity;

public class IngredientDTO {

    private Long id;
    private String description;
    private String name;

    public IngredientDTO() {
        // Empty constructor needed for Jackson.
    }

    public IngredientDTO(IngredientEntity ingredientEntity) {
        this.id = ingredientEntity.getId();
        this.name = ingredientEntity.getName();
        this.description = ingredientEntity.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "IngredientDTO{" +
            "id=" + id +
            ", description='" + description + '\'' +
            ", name='" + name + '\'' +
            '}';
    }
}
