package pl.recipes.service.dto;

import pl.recipes.domain.RecipeStepEntity;

public class RecipeStepDTO {

    private Long id;
    private String description;
    private Integer sortableNumber;
    private String photo;
    private Long recipeId;


    public RecipeStepDTO() {
        // Empty constructor needed for Jackson.
    }

    public RecipeStepDTO(RecipeStepEntity recipeStepEntity) {
        this.id = recipeStepEntity.getId();
        this.description = recipeStepEntity.getDescription();
        this.sortableNumber = recipeStepEntity.getSortableNumber();
        this.photo = recipeStepEntity.getPhoto();
        this.recipeId = recipeStepEntity.getRecipe().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSortableNumber() {
        return sortableNumber;
    }

    public void setSortableNumber(Integer sortableNumber) {
        this.sortableNumber = sortableNumber;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public String toString() {
        return "RecipeStepDTO{" +
            "id=" + id +
            ", description='" + description + '\'' +
            ", sortableNumber=" + sortableNumber +
            ", photo='" + photo + '\'' +
            ", recipe=" + recipeId +
            '}';
    }
}
