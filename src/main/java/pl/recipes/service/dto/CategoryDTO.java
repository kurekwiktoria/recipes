package pl.recipes.service.dto;

import pl.recipes.domain.CategoryEntity;

public class CategoryDTO {

    private Long id;

    private String name;

    private Long mainCategoryId;

    public CategoryDTO() {
        // Empty constructor needed for Jackson.
    }

    public CategoryDTO(CategoryEntity categoryEntity) {
       if(categoryEntity != null) {
           this.id = categoryEntity.getId();
           this.name = categoryEntity.getName();
           this.mainCategoryId = categoryEntity.getMainCategoryId();
       }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(Long mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    @Override
    public String toString() {
        return "CategoryDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }
}
