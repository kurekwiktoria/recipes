package pl.recipes.service.dto;

import pl.recipes.domain.RateEntity;
import pl.recipes.domain.RecipeEntity;

import java.util.Collections;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

public class RecipeDTO {

    private Long id;
    private String title;
    private String userLogin;
    private Integer portions;
    private PreparationTimeDTO preparationTime;
    private String difficultyLevel;
    private String expense;
    private String photoPath;
    private String videoPath;
    private Integer rate;
    private List<CategoryDTO> categories;
    private List<KeywordDTO> keywords;
    private List<RecipeIngredientDTO> recipeIngredients;
    private List<RecipeStepDTO> steps;
    private List<CommentDTO> comments;
    private List<RecipesSimilarityDTO> recipesSimilarity;
    private List<PreferenceDTO> preferences;

    public RecipeDTO() {
        // Empty constructor needed for Jackson.
    }

    public RecipeDTO(RecipeEntity recipeEntity) {
        this.id = recipeEntity.getId();
        this.title = recipeEntity.getTitle();
        this.userLogin = recipeEntity.getUserEntity().getLogin();
        this.portions = recipeEntity.getPortions();
        this.preparationTime = new PreparationTimeDTO(recipeEntity.getPreparationTime());
        this.difficultyLevel = recipeEntity.getDifficultyLevel().name();
        this.expense = recipeEntity.getExpense().name();
        this.photoPath = recipeEntity.getPhotoPath();
        this.videoPath = recipeEntity.getVideoPath();
        this.categories = !recipeEntity.getCategories().isEmpty() ?
            recipeEntity.getCategories().stream().map(CategoryDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();
        this.keywords = !recipeEntity.getKeywords().isEmpty() ?
            recipeEntity.getKeywords().stream().map(KeywordDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();
        this.recipeIngredients = !recipeEntity.getRecipeIngredients().isEmpty() ?
            recipeEntity.getRecipeIngredients().stream().map(RecipeIngredientDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();
        this.steps = !recipeEntity.getSteps().isEmpty() ?
            recipeEntity.getSteps().stream().map(RecipeStepDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();
        this.comments = !recipeEntity.getComments().isEmpty() ?
            recipeEntity.getComments().stream().map(CommentDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();

        if(!recipeEntity.getRates().isEmpty()){
            IntSummaryStatistics stats = recipeEntity.getRates().stream()
                .mapToInt(RateEntity::getValue)
                .summaryStatistics();
            this.rate = Math.toIntExact(Math.round(stats.getAverage()));
        }
        else {
            this.rate = 0;
        }
        this.recipesSimilarity = !recipeEntity.getRecipesSimilarity().isEmpty() ?
            recipeEntity.getRecipesSimilarity().stream().map(RecipesSimilarityDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();
        this.preferences = !recipeEntity.getPreferences().isEmpty() ?
            recipeEntity.getPreferences().stream().map(PreferenceDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Integer getPortions() {
        return portions;
    }

    public void setPortions(Integer portions) {
        this.portions = portions;
    }

    public PreparationTimeDTO getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(PreparationTimeDTO preparationTime) {
        this.preparationTime = preparationTime;
    }

    public String getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(String difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public List<CategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDTO> categories) {
        this.categories = categories;
    }

    public List<KeywordDTO> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<KeywordDTO> keywords) {
        this.keywords = keywords;
    }

    public List<RecipeIngredientDTO> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(List<RecipeIngredientDTO> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    public List<RecipeStepDTO> getSteps() {
        return steps;
    }

    public void setSteps(List<RecipeStepDTO> steps) {
        this.steps = steps;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public List<RecipesSimilarityDTO> getRecipesSimilarity() {
        return recipesSimilarity;
    }

    public void setRecipesSimilarity(List<RecipesSimilarityDTO> recipesSimilarity) {
        this.recipesSimilarity = recipesSimilarity;
    }

    public List<PreferenceDTO> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<PreferenceDTO> preferences) {
        this.preferences = preferences;
    }

    @Override
    public String toString() {
        return "RecipeDTO{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", user=" + userLogin +
            ", portions=" + portions +
            ", preparationTime=" + preparationTime +
            ", difficultyLevel='" + difficultyLevel + '\'' +
            ", expense='" + expense + '\'' +
            ", photoPath='" + photoPath + '\'' +
            ", videoPath='" + videoPath + '\'' +
            ", categories=" + categories +
            ", keywords=" + keywords +
            ", recipeIngredients=" + recipeIngredients +
            ", steps=" + steps +
            ", comments=" + comments +
            ", recipesSimilarity=" + recipesSimilarity +
            ", preferences=" + preferences +
            '}';
    }
}
