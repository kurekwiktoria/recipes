package pl.recipes.service.dto;

import pl.recipes.domain.MeasureEntity;

public class MeasureDTO {

    private Long id;
    private String type;
    private String description;

    public MeasureDTO() {
        // Empty constructor needed for Jackson.
    }

    public MeasureDTO(MeasureEntity measureEntity) {
        this.id = measureEntity.getId();
        this.type = measureEntity.getType();
        this.description = measureEntity.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "MeasureDTO{" +
            "id=" + id +
            ", type='" + type + '\'' +
            ", description='" + description + '\'' +
            '}';
    }
}
