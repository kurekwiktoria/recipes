package pl.recipes.service.dto;

import pl.recipes.domain.RecipeIngredientEntity;

public class RecipeIngredientDTO {

    private Long id;
    private Long recipeId;
    private Long ingredientId;
    private IngredientDTO ingredient;
    private MeasureDTO measure;
    private Float quantity;
    private String description;

    public RecipeIngredientDTO() {
        // Empty constructor needed for Jackson.
    }

    public RecipeIngredientDTO(RecipeIngredientEntity recipeIngredientEntity) {
        this.recipeId = recipeIngredientEntity.getRecipe().getId();
        this.ingredientId = recipeIngredientEntity.getIngredient().getId();
        this.measure = new MeasureDTO(recipeIngredientEntity.getMeasure());
        this.ingredient = new IngredientDTO(recipeIngredientEntity.getIngredient());
        this.quantity = recipeIngredientEntity.getQuantity();
        this.description = recipeIngredientEntity.getDescription();
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public MeasureDTO getMeasure() {
        return measure;
    }

    public void setMeasure(MeasureDTO measure) {
        this.measure = measure;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IngredientDTO getIngredient() {
        return ingredient;
    }

    public void setIngredient(IngredientDTO ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public String toString() {
        return "RecipeIngredientDTO{" +
            "recipeId=" + recipeId +
            ", ingredientId=" + ingredientId +
            ", measure=" + measure +
            ", quantity=" + quantity +
            ", description='" + description + '\'' +
            '}';
    }
}
