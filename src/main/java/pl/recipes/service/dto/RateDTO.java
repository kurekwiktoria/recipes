package pl.recipes.service.dto;

import pl.recipes.domain.RateEntity;

public class RateDTO {

    private Long recipeId;
    private String userLogin;
    private Integer value;

    public RateDTO() {
        // Empty constructor needed for Jackson.
    }

    public RateDTO(RateEntity rateEntity) {
        this.userLogin = rateEntity.getUser().getLogin();
        this.recipeId = rateEntity.getRecipe().getId();
        this.value = rateEntity.getValue();
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "RateDTO{" +
            "recipeId=" + recipeId +
            ", recipe=" + recipeId +
            ", value=" + value +
            '}';
    }
}
