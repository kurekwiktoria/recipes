package pl.recipes.service.dto;

import pl.recipes.domain.CommentEntity;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CommentDTO {

    private Long id;
    private String userLogin;
    private Long recipeId;
    private String type;
    private String description;
    private Long commentId;
    private List<CommentDTO> replies;

    public CommentDTO() {
        // Empty constructor needed for Jackson.
    }

    public CommentDTO(CommentEntity commentEntity) {
        this.id = commentEntity.getId();
        this.userLogin = commentEntity.getUser().getLogin();
        this.recipeId = commentEntity.getRecipe().getId();
        this.type = commentEntity.getType().name();
        this.description = commentEntity.getDescription();
        this.commentId = commentEntity.getComment() != null ?
            commentEntity.getComment().getId() : null;
        this.replies = !commentEntity.getReplies().isEmpty() ?
            commentEntity.getReplies().stream().map(CommentDTO::new).collect(Collectors.toList()) :
            Collections.emptyList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public List<CommentDTO> getReplies() {
        return replies;
    }

    public void setReplies(List<CommentDTO> replies) {
        this.replies = replies;
    }

    @Override
    public String toString() {
        return "CommentDTO{" +
            "id=" + id +
            ", user=" + userLogin +
            ", recipe=" + recipeId +
            ", type='" + type + '\'' +
            ", description='" + description + '\'' +
            ", comment=" + commentId +
            ", replies=" + replies +
            '}';
    }
}
