package pl.recipes.service.dto;

import pl.recipes.domain.RecipesSimilarityEntity;

public class RecipesSimilarityDTO {

    private Long firstRecipeId;
    private Long secondRecipeId;
    private Float similarity;

    public RecipesSimilarityDTO() {
        // Empty constructor needed for Jackson.
    }

    public RecipesSimilarityDTO(RecipesSimilarityEntity recipesSimilarityEntity) {
        this.firstRecipeId = recipesSimilarityEntity.getId().getRecipe1Id();
        this.secondRecipeId = recipesSimilarityEntity.getId().getRecipe2Id();
        this.similarity = recipesSimilarityEntity.getSimilarity();
    }

    public Long getFirstRecipeId() {
        return firstRecipeId;
    }

    public void setFirstRecipeId(Long firstRecipeId) {
        this.firstRecipeId = firstRecipeId;
    }

    public Long getSecondRecipeId() {
        return secondRecipeId;
    }

    public void setSecondRecipeId(Long secondRecipeId) {
        this.secondRecipeId = secondRecipeId;
    }

    public Float getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Float similarity) {
        this.similarity = similarity;
    }

    @Override
    public String toString() {
        return "RecipesSimilarityDTO{" +
            "firstRecipeId=" + firstRecipeId +
            ", secondRecipeId=" + secondRecipeId +
            ", similarity=" + similarity +
            '}';
    }
}
