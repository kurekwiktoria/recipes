package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= MeasureEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MeasureEntity implements Serializable {

    public static final String TABLE_NAME = "measure";
    public static final String COL_ID = "id";
    private static final String COL_TYPE = "type";
    private static final String COL_DESCRIPTION = "description";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_TYPE)
    private String type;

    @Column(name = COL_DESCRIPTION)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        return "Measure [id=" + id + ", type=" + type +
                ", description=" + description +"]";
    }

    public MeasureEntity(String type, String description){
        this.type = type;
        this.description = description;
    }

    public MeasureEntity(){
        super();
    }
}
