package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= RecipeStepEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecipeStepEntity implements Serializable {

    public static final String TABLE_NAME = "recipe_steps";
    public static final String COL_ID = "id";
    private static final String REFCOL_RECIPE = "recipe_id";
    private static final String COL_DESCRIPTION = "description";
    private static final String COL_SORTABLE = "sortable_numer";
    private static final String COL_PHOTO = "photo_path";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_DESCRIPTION)
    private String description;

    @Column(name = COL_SORTABLE)
    private Integer sortableNumber;

    @Column(name = COL_PHOTO)
    private String photo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id")
    private RecipeEntity recipe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSortableNumber() {
        return sortableNumber;
    }

    public void setSortableNumber(Integer sortableNumber) {
        this.sortableNumber = sortableNumber;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public RecipeEntity getRecipe() {
        return recipe;
    }

    public void setRecipe(RecipeEntity recipe) {
        this.recipe = recipe;
    }

    @Override
    public String toString(){
        return "RecipeStep [id=" + id +
                ", description=" + description + "]";
    }


    public RecipeStepEntity(){
        super();
    }
}
