package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.recipes.service.util.PreparationTimeEnum;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= PreparationTimeEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PreparationTimeEntity implements Serializable {

    public static final String TABLE_NAME = "preparation_time";
    public static final String COL_ID = "id";
    private static final String COL_QUANTITY = "quantity";
    private static final String COL_TYPE = "type";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_QUANTITY)
    private Integer quantity;

    @Column(name = COL_TYPE)
    @Enumerated(EnumType.STRING)
    private PreparationTimeEnum type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public PreparationTimeEnum getType() {
        return type;
    }

    public void setType(PreparationTimeEnum type) {
        this.type = type;
    }

    @Override
    public String toString(){
        return "Preparation time [id=" + id + ", quantity=" + quantity +
                "type=" + type.getHumanReadableDescription() + "]";
    }

    public PreparationTimeEntity(Integer quantity, PreparationTimeEnum type){
        this.quantity = quantity;
        this.type = type;
    }

    public PreparationTimeEntity(){
        super();
    }
}
