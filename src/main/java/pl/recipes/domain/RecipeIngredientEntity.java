package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.recipes.domain.key.RecipeIngredientKey;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= RecipeIngredientEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecipeIngredientEntity implements Serializable {

    public static final String TABLE_NAME = "recipe_ingredients";
    public static final String COL_ID = "id";
    private static final String REFCOL_RECIPE = "recipe_id";
    private static final String REFCOL_INGREDIENT = "ingredient_id";
    private static final String REFCOL_MEASURE = "measure_id";
    private static final String COL_QUANTITY = "quantity";
    private static final String COL_DESCRIPTION = "description";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "recipe_id")
   private RecipeEntity recipe;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "ingredient_id")
   private IngredientEntity ingredient;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "measure_id")
    private MeasureEntity measure;

    @Column(name = COL_QUANTITY)
    private Float quantity;

    @Column(name = COL_DESCRIPTION)
    private String description;

    public RecipeEntity getRecipe() {
        return recipe;
    }

    public void setRecipe(RecipeEntity recipe) {
        this.recipe = recipe;
    }

    public IngredientEntity getIngredient() {
        return ingredient;
    }

    public void setIngredient(IngredientEntity ingredient) {
        this.ingredient = ingredient;
    }

    public MeasureEntity getMeasure() {
        return measure;
    }

    public void setMeasure(MeasureEntity measure) {
        this.measure = measure;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString(){
        return "RecipeIngredients [id=" + id + ", recipe=" + recipe.getTitle()+
                ", category=" + ingredient.getName() + "]";
    }


    public RecipeIngredientEntity(){
        super();
    }
}
