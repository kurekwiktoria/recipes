package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name=CategoryEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CategoryEntity implements Serializable {

    public static final String TABLE_NAME = "categories";
    public static final String COL_ID = "id";
    private static final String COL_NAME = "name";
    private static final String COL_MAIN_CATEGORY = "main_category_id";


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_NAME)
    private String name;

    @Column(name = COL_MAIN_CATEGORY)
    private Long mainCategoryId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(Long mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    @Override
    public String toString(){
        return "Category [id=" + id + ", name=" + name + "]";
    }

    public CategoryEntity(String name){
        this.name = name;
    }

    public CategoryEntity(){
        super();
    }


}
