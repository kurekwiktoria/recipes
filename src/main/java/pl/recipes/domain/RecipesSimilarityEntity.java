package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.recipes.domain.key.RecipesSimilarityKey;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= RecipesSimilarityEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecipesSimilarityEntity implements Serializable {

    public static final String TABLE_NAME = "recipe_recipe_similarity";
    public static final String COL_ID = "id";
    private static final String REFCOL_RECIPE1 = "recipe1_id";
    private static final String REFCOL_RECIPE2 = "recipe2_id";
    private static final String COL_SIMILARITY = "similarity";

    @EmbeddedId
    private RecipesSimilarityKey id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("recipe1_id")
    @JoinColumn(name = "recipe1_id")
    private RecipeEntity firstRecipe;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("recipe2_id")
    @JoinColumn(name = "recipe2_id")
    private RecipeEntity secondRecipe;

    @Column(name = COL_SIMILARITY)
    private Float similarity;

    public RecipesSimilarityKey getId() {
        return id;
    }

    public void setId(RecipesSimilarityKey id) {
        this.id = id;
    }

    public RecipeEntity getFirstRecipe() {
        return firstRecipe;
    }

    public void setFirstRecipe(RecipeEntity firstRecipe) {
        this.firstRecipe = firstRecipe;
    }

    public RecipeEntity getSecondRecipe() {
        return secondRecipe;
    }

    public void setSecondRecipe(RecipeEntity secondRecipe) {
        this.secondRecipe = secondRecipe;
    }

    public Float getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Float similarity) {
        this.similarity = similarity;
    }
}
