package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name= FavouriteEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FavouriteEntity implements Serializable {

    public static final String TABLE_NAME = "favourite";
    public static final String COL_ID = "id";
    private static final String COL_NAME = "name";
    private static final String REFCOL_USER = "user_id";
    private static final String COL_DESCRIPTION = "description";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_NAME)
    private String name;

    @ManyToOne
    @JoinColumn(name = REFCOL_USER)
    private UserEntity user;

    @Column(name = COL_DESCRIPTION)
    private String description;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "favourite_recipe",
        joinColumns = {@JoinColumn(name = "favourite_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "recipe_id", referencedColumnName = "id")})
    private List<RecipeEntity> recipes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RecipeEntity> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<RecipeEntity> recipes) {
        this.recipes = recipes;
    }

    @Override
    public String toString(){
        return "Favourite [id=" + id + ", name=" + name+"]";
    }


    public FavouriteEntity(){
        super();
    }
}
