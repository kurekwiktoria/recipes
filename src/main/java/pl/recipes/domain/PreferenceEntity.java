package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.recipes.domain.key.PreferencesKey;
import pl.recipes.service.util.PreferencesTypeEnum;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= PreferenceEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PreferenceEntity implements Serializable {

    public static final String TABLE_NAME = "preferences";
    public static final String COL_ID = "id";
    private static final String REFCOL_USER = "user_id";
    private static final String REFCOL_RECIPE = "recipe_id";
    private static final String COL_TYPE = "type";
    private static final String COL_DESCRIPTION = "description";


   @EmbeddedId
   private PreferencesKey id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("recipe_id")
    @JoinColumn(name = "recipe_id")
    private RecipeEntity recipe;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("user_id")
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = COL_TYPE)
    @Enumerated(EnumType.STRING)
    private PreferencesTypeEnum type;

    @Column(name = COL_DESCRIPTION)
    private String description;

    public PreferencesKey getId() {
        return id;
    }

    public void setId(PreferencesKey id) {
        this.id = id;
    }

    public RecipeEntity getRecipe() {
        return recipe;
    }

    public void setRecipe(RecipeEntity recipe) {
        this.recipe = recipe;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public PreferencesTypeEnum getType() {
        return type;
    }

    public void setType(PreferencesTypeEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        return "Preference [id=" + id + ", type=" + type.getHumanReadableDescription() + //
        ", description=" + description +"]";
    }
}
