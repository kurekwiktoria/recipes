package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= KeywordEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class KeywordEntity implements Serializable {

    public static final String TABLE_NAME = "keyword";
    public static final String COL_ID = "id";
    private static final String COL_NAME = "name";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_NAME)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return "Keyword [id=" + id + ", name=" + name + "]";
    }

    public KeywordEntity(String name){
        this.name = name;
    }

    public KeywordEntity(){
        super();
    }
}
