package pl.recipes.domain.key;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RecipesSimilarityKey implements Serializable {
    @Column(name = "recipe1_id")
    private Long recipe1Id;

    @Column(name = "recipe2_id")
    private Long recipe2Id;

    public Long getRecipe1Id() {
        return recipe1Id;
    }

    public void setRecipe1Id(Long recipe1Id) {
        this.recipe1Id = recipe1Id;
    }

    public Long getRecipe2Id() {
        return recipe2Id;
    }

    public void setRecipe2Id(Long recipe2Id) {
        this.recipe2Id = recipe2Id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipesSimilarityKey that = (RecipesSimilarityKey) o;
        return Objects.equals(recipe1Id, that.recipe1Id) &&
            Objects.equals(recipe2Id, that.recipe2Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipe1Id, recipe2Id);
    }
}
