package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name= RateEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RateEntity implements Serializable {

    public static final String TABLE_NAME = "rate";
    public static final String COL_ID = "id";
    private static final String REFCOL_USER = "user_id";
    private static final String REFCOL_RECIPE = "recipe_id";
    private static final String COL_VALUE = "value";


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id")
    private RecipeEntity recipe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = COL_VALUE)
    private Integer value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RecipeEntity getRecipe() {
        return recipe;
    }

    public void setRecipe(RecipeEntity recipe) {
        this.recipe = recipe;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString(){
        return "Rate [id=" + id + ", value=" + value+"]";
    }


    public RateEntity(){
        super();
    }
}
