package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.recipes.service.util.DifficultyLevelEnum;
import pl.recipes.service.util.ExpenseEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name= RecipeEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecipeEntity implements Serializable {

    public static final String TABLE_NAME = "recipe";
    public static final String COL_ID = "id";
    private static final String COL_TITLE = "title";
    private static final String REFCOL_USER = "user_id";
    private static final String COL_PORTIONS = "portions";
    private static final String REFCOL_PREPARATION_TIME = "preparation_time_id";
    private static final String COL_DIFFICULTY_LEVEL = "difficulty_level";
    private static final String COL_EXPENSE = "expense";
    private static final String COL_PHOTO = "photo_path";
    private static final String COL_VIDEO = "video_path";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @Column(name = COL_TITLE)
    private String title;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = COL_PORTIONS)
    private Integer portions;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "preparation_time_id")
    private PreparationTimeEntity preparationTime;

    @Column(name = COL_DIFFICULTY_LEVEL)
    @Enumerated(EnumType.STRING)
    private DifficultyLevelEnum difficultyLevel;

    @Column(name = COL_EXPENSE)
    @Enumerated(EnumType.STRING)
    private ExpenseEnum expense;

    @Column(name = COL_PHOTO)
    private String photoPath;

    @Column(name = COL_VIDEO)
    private String videoPath;

    @Column(name = "active")
    private boolean active = true;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "recipe_categories",
        joinColumns = {@JoinColumn(name = "recipe_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "category_id", referencedColumnName = "id")})
    private List<CategoryEntity> categories;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "recipe_keywords",
        joinColumns = {@JoinColumn(name = "recipe_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "keyword_id", referencedColumnName = "id")})
    private List<KeywordEntity> keywords;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "favourite_recipe",
        joinColumns = {@JoinColumn(name = "recipe_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "favourite_id", referencedColumnName = "id")})
    private List<FavouriteEntity> favourites;

    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY)
    private List<RecipeIngredientEntity> recipeIngredients;

    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY)
    private List<RecipeStepEntity> steps;

    @JsonIgnore
    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY)
    private List<CommentEntity> comments;

    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY)
    private List<RateEntity> rates;

    @OneToMany(mappedBy = "firstRecipe", fetch = FetchType.LAZY)
    private List<RecipesSimilarityEntity> recipesSimilarity;

    @OneToMany(mappedBy = "recipe", fetch = FetchType.LAZY)
    private List<PreferenceEntity> preferences;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserEntity getUserEntity() {
        return user;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.user = userEntity;
    }

    public Integer getPortions() {
        return portions;
    }

    public void setPortions(Integer portions) {
        this.portions = portions;
    }

    public PreparationTimeEntity getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(PreparationTimeEntity preparationTime) {
        this.preparationTime = preparationTime;
    }

    public DifficultyLevelEnum getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(DifficultyLevelEnum difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public ExpenseEnum getExpense() {
        return expense;
    }

    public void setExpense(ExpenseEnum expense) {
        this.expense = expense;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public List<CategoryEntity> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryEntity> categories) {
        this.categories = categories;
    }

    public List<KeywordEntity> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<KeywordEntity> keywords) {
        this.keywords = keywords;
    }

    public List<RecipeIngredientEntity> getRecipeIngredients() {
        return recipeIngredients;
    }

    public List<RecipeStepEntity> getSteps() {
        return steps;
    }

    public List<CommentEntity> getComments() {
        return comments;
    }

    public List<RateEntity> getRates() {
        return rates;
    }

    public List<RecipesSimilarityEntity> getRecipesSimilarity() {
        return recipesSimilarity;
    }

    public List<PreferenceEntity> getPreferences() {
        return preferences;
    }

    public List<FavouriteEntity> getFavourites() {
        return favourites;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString(){
        return "Recipe [id=" + id + ", title=" + title+"]";
    }


    public RecipeEntity(){
        super();
    }
}
