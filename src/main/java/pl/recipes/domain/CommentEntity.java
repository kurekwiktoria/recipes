package pl.recipes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.recipes.service.util.CommentTypeEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name=CommentEntity.TABLE_NAME)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CommentEntity implements Serializable {

    public static final String TABLE_NAME = "comment";
    public static final String COL_ID = "id";
    private static final String REFCOL_USER = "user_id";
    private static final String REFCOL_RECIPE = "recipe_id";
    private static final String REFCOL_COMMENT = "comment_id";
    private static final String COL_TYPE = "type";
    private static final String COL_DESCRIPTION = "description";


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = COL_ID)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id")
    private RecipeEntity recipe;

    @Column(name = COL_TYPE)
    @Enumerated(EnumType.STRING)
    private CommentTypeEnum type;

    @Column(name = COL_DESCRIPTION)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private CommentEntity comment;

    @OneToMany(mappedBy = "comment")
    private List<CommentEntity> replies;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public CommentTypeEnum getType() {
        return type;
    }

    public void setType(CommentTypeEnum type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CommentEntity> getReplies() {
        return replies;
    }

    public void setReplies(List<CommentEntity> replies) {
        this.replies = replies;
    }

    public RecipeEntity getRecipe() {
        return recipe;
    }

    public void setRecipe(RecipeEntity recipe) {
        this.recipe = recipe;
    }

    public CommentEntity getComment() {
        return comment;
    }

    public void setComment(CommentEntity comment) {
        this.comment = comment;
    }

    @Override
    public String toString(){
        return "Comment";
    }


    public CommentEntity(){
        super();
    }
}
