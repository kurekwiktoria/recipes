package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.CommentEntity;

import java.util.List;
@Repository
public interface CommentRepository extends JpaRepository<CommentEntity, Long> {

    public List<CommentEntity> findCommentEntitiesByRecipeId(Long recipeId);
    public List<CommentEntity> findCommentEntitiesByCommentId(Long commentId);
}
