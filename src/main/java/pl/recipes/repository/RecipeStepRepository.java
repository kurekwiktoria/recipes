package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.RecipeStepEntity;

import java.util.List;
@Repository
public interface RecipeStepRepository extends JpaRepository<RecipeStepEntity, Long> {

    public List<RecipeStepEntity> findRecipeStepEntitiesByRecipeIdOrderBySortableNumberAsc(Long id);

    public RecipeStepEntity findRecipeStepEntityByRecipeIdOrderBySortableNumberDesc(Long id);
}
