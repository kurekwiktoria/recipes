package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.RecipeEntity;

import java.util.Collection;
import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<RecipeEntity, Long> {

    List<RecipeEntity> findAllByCategoriesIdInAndActiveTrue(Collection<Long> ids);

    List<RecipeEntity> findTop20ByPhotoPathIsNotNullAndActiveTrueOrderByIdDesc();

    @Query("SELECT DISTINCT recipe FROM RecipeEntity recipe " +
        "   LEFT JOIN recipe.categories AS category " +
        "   LEFT JOIN recipe.keywords AS keyword " +
        "   LEFT JOIN recipe.recipeIngredients AS recipeIngredients " +
        "   LEFT JOIN recipeIngredients.ingredient AS ingredient " +
        "   LEFT JOIN CategoryEntity mainCategory ON mainCategory.id = category.mainCategoryId" +
        "   WHERE " +
        "       (LOWER(recipe.title) LIKE LOWER(CONCAT('%', CONCAT(?1, '%')))" +
        "       OR  LOWER(category.name) LIKE LOWER(CONCAT('%', CONCAT(?1, '%')))" +
        "       OR  LOWER(keyword.name) LIKE LOWER(CONCAT('%', CONCAT(?1, '%')))" +
        "       OR  LOWER(ingredient.name) LIKE LOWER(CONCAT('%', CONCAT(?1, '%')))" +
        "       OR  LOWER(mainCategory.name) LIKE LOWER(CONCAT('%', CONCAT(?1, '%'))))" +
        "       AND recipe.active = true" +
        "           ")
    List<RecipeEntity> searchRecipes(String params);

}
