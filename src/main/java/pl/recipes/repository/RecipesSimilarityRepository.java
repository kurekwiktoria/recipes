package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.RecipesSimilarityEntity;
@Repository
public interface RecipesSimilarityRepository extends JpaRepository<RecipesSimilarityEntity, Long> {

}
