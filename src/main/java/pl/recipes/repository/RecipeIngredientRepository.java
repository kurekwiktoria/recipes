package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.RecipeIngredientEntity;

import java.util.List;
@Repository
public interface RecipeIngredientRepository extends JpaRepository<RecipeIngredientEntity, Long> {

    public List<RecipeIngredientEntity> findRecipeIngredientEntitiesByRecipeId(Long id);

    public List<RecipeIngredientEntity> findRecipeIngredientEntitiesByIngredientId(Long id);
}
