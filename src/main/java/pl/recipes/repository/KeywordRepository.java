package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.KeywordEntity;

import java.util.Optional;

@Repository
public interface KeywordRepository extends JpaRepository<KeywordEntity, Long> {
    Optional<KeywordEntity> findByNameEquals(String name);
}
