package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.RecipeEntity;

import java.util.List;

@Repository
public interface RecommendationRepository extends JpaRepository<RecipeEntity, Long> {

    @Query("SELECT DISTINCT r FROM RecipeEntity r " +
        "   JOIN r.rates AS ra " +
        "   WHERE ra.user.id IN " +
        "   (" +
        "       SELECT ra.user.id" +
        "       FROM RecipeEntity r " +
        "       JOIN r.rates ra" +
        "           WHERE r.id IN " +
        "           (" +
        "               SELECT r.id" +
        "               FROM RecipeEntity r" +
        "               JOIN r.rates ra" +
        "                   WHERE ra.user.login = (?1) AND ra.value >= 4" +
        "           )" +
        "   )  " +
        "   AND r.id NOT IN " +
        "   (" +
        "       SELECT r.id" +
        "       FROM RecipeEntity r" +
        "       JOIN r.rates ra" +
        "       WHERE ra.user.login = (?1)" +
        "     )" +
        "           ")
    List<RecipeEntity> getRecommendations(String userLogin);

}
