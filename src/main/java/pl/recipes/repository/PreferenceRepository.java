package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.PreferenceEntity;

import java.util.List;
@Repository
public interface PreferenceRepository extends JpaRepository<PreferenceEntity, Long> {

    public List<PreferenceEntity> findPreferenceEntitiesByUserId(Long id);

}
