package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.CategoryEntity;

import java.util.List;
@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
    List<CategoryEntity> findAllByOrderByName();
    CategoryEntity findByNameEqualsIgnoreCase(String name);
    List<CategoryEntity> findAllByMainCategoryIdEquals(Long mainCategoryId);

    @Query("select c.id from CategoryEntity c where c.mainCategoryId = ?1")
    List<Long> getAllIdsByMainCategoryId(Long mainCategoryId);
}
