package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.IngredientEntity;
@Repository
public interface IngredientRepository extends JpaRepository<IngredientEntity, Long> {

}
