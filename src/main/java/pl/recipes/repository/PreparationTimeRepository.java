package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.PreparationTimeEntity;
import pl.recipes.service.util.PreparationTimeEnum;

import java.util.Optional;

@Repository
public interface PreparationTimeRepository extends JpaRepository<PreparationTimeEntity, Long> {

    Optional<PreparationTimeEntity> findByQuantityEqualsAndTypeEquals(Integer quantity, PreparationTimeEnum type);

}
