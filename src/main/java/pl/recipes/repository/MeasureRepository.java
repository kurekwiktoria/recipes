package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.MeasureEntity;
@Repository
public interface MeasureRepository extends JpaRepository<MeasureEntity, Long> {

}
