package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.FavouriteEntity;

import java.util.List;

@Repository
public interface FavouriteRepository extends JpaRepository<FavouriteEntity, Long> {

    public List<FavouriteEntity> findByUserLoginEquals(String login);

}
