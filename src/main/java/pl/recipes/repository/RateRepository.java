package pl.recipes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.recipes.domain.RateEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface RateRepository extends JpaRepository<RateEntity, Long> {

    public List<RateEntity> findRateEntitiesByRecipeId(Long id);

    public Optional<RateEntity> findByRecipeIdEqualsAndUserLoginEquals(Long recipeId, String userLogin);
}
