import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import {
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatStepperModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { Ng5SliderModule } from 'ng5-slider';
import { RatingModule } from 'ng-starrating';
import { MatSelectSearchModule } from 'app/shared/select-with-search/mat-select-search.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { RecommendationsList } from 'app/recommendations/recommendations-list/recommendations-list.component';
import { recommendationsRoute } from 'app/recommendations/recommendations.route';

@NgModule({
    declarations: [RecommendationsList],
    imports: [
        CommonModule,
        RecipesSharedModule,
        MatStepperModule,
        MatFormFieldModule,
        RouterModule.forChild(recommendationsRoute),
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatSliderModule,
        MatSelectModule,
        NgxMatSelectSearchModule,
        MatIconModule,
        Ng5SliderModule,
        MatRadioModule,
        MatChipsModule,
        MatGridListModule,
        RatingModule,
        MatCardModule,
        MatSelectSearchModule,
        MatExpansionModule,
        MatDividerModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecommendationsModule {}
