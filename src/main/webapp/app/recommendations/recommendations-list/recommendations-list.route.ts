import { Route } from '@angular/router';
import { RecommendationsList } from 'app/recommendations/recommendations-list/recommendations-list.component';

export const recommendationsListRoute: Route = {
    path: '',
    component: RecommendationsList,
    data: {
        pageTitle: 'recommendations.list.title'
    }
};
