import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from 'app/core/service/recipe.service';
import { Recipe } from 'app/core/model/recipe.model';
import { SERVER_API_URL } from 'app/app.constants';
import { AccountService } from 'app/core';

@Component({
    selector: 'jhi-recommendations-list',
    templateUrl: './recommendations-list.component.html',
    styles: []
})
export class RecommendationsList implements OnInit {
    recipes: Recipe[];
    account: Account;
    downloadUrl = SERVER_API_URL + '/api/downloadFile/?path=';

    @Input() searchParams: string;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private recipeService: RecipeService,
        private accountService: AccountService
    ) {}

    ngOnInit() {
        this.accountService.identity().then(account => {
            this.account = account;
            this.recipeService.getRecommendations(account.login).subscribe(recipes => {
                this.recipes = recipes.body;
            });
        });
    }

    goToDetails(id: any) {
        this.router.navigate(['../przepisy/szczegoly/' + id], { relativeTo: this.route.parent });
    }
}
