import { Routes } from '@angular/router';
import { recommendationsListRoute } from 'app/recommendations/recommendations-list/recommendations-list.route';

const RECOMEMNDATIONS_ROUTE = [recommendationsListRoute];

export const recommendationsRoute: Routes = [
    {
        path: '',
        children: RECOMEMNDATIONS_ROUTE
    }
];
