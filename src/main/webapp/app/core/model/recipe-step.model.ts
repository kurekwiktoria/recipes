import { Recipe } from 'app/core/model/recipe.model';

export interface IRecipeStep {
    id?: any;
    description?: string;
    sortableNumber?: number;
    photo?: string;
    recipe?: Recipe;
    recipeId?: any;
    fileData?: File;
    previewUrl?: any;
}

export class RecipeStep implements IRecipeStep {
    constructor(
        public id?: any,
        public description?: string,
        public sortableNumber?: number,
        public photo?: string,
        public recipeId?: any,
        public fileData?: File,
        public previewUrl?: any
    ) {
        this.id = id ? id : null;
        this.description = description ? description : null;
        this.sortableNumber = sortableNumber ? sortableNumber : null;
        this.photo = photo ? photo : null;
        this.recipeId = recipeId ? recipeId : null;
        this.fileData = fileData ? fileData : null;
        this.previewUrl = previewUrl ? previewUrl : null;
    }
}
