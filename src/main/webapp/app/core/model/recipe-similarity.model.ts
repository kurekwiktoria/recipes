export interface IRecipeSimilarity {
    firstRecipeId?: any;
    secondRecipeId?: any;
    similarity?: number;
}

export class RecipeSimilarity implements IRecipeSimilarity {
    constructor(public firstRecipeId?: any, public secondRecipeId?: any, public similarity?: number) {
        this.firstRecipeId = firstRecipeId ? firstRecipeId : null;
        this.secondRecipeId = secondRecipeId ? secondRecipeId : null;
        this.similarity = similarity ? similarity : null;
    }
}
