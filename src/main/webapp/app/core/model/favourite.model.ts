import { Recipe } from 'app/core/model/recipe.model';

export interface IFavourite {
    id?: any;
    userLogin?: string;
    name?: string;
    description?: string;
    recipes?: Recipe[];
}

export class Favourite implements IFavourite {
    constructor(public id?: any, public userLogin?: string, public name?: string, public description?: string, public recipes?: Recipe[]) {
        this.id = id ? id : null;
        this.userLogin = userLogin ? userLogin : null;
        this.name = name ? name : null;
        this.description = description ? description : null;
        let r: Recipe[];
        this.recipes = recipes ? recipes : r;
    }
}
