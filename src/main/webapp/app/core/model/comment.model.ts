export interface IComment {
    id?: any;
    userLogin?: string;
    recipeId?: any;
    type?: string;
    description?: string;
    commentId?: any;
    replies?: Comment[];
}

export class Comment implements IComment {
    constructor(
        public id?: any,
        public userLogin?: string,
        public recipeId?: any,
        public type?: string,
        public description?: string,
        public commentId?: any,
        public replies?: Comment[]
    ) {
        this.id = id ? id : null;
        this.userLogin = userLogin ? userLogin : null;
        this.recipeId = recipeId ? recipeId : null;
        this.type = type ? type : null;
        this.description = description ? description : null;
        this.commentId = commentId ? commentId : null;
        let c: Comment[];
        this.replies = replies ? replies : c;
    }
}
