export interface IMeasure {
    id?: any;
    type?: string;
    description?: string;
}

export class Measure implements IMeasure {
    constructor(public id?: any, public type?: string, public description?: string) {
        this.id = id ? id : null;
        this.type = type ? type : null;
        this.description = description ? description : null;
    }
}
