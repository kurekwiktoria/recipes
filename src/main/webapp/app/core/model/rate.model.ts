import { Recipe } from 'app/core/model/recipe.model';
import { User } from 'app/core';

export interface IRate {
    recipeId?: number;
    userLogin?: string;
    value?: number;
}

export class Rate implements IRate {
    constructor(public recipeId?: number, public userLogin?: string, public recipe?: Recipe, public user?: User, public value?: number) {
        this.recipeId = recipeId ? recipeId : null;
        this.userLogin = userLogin ? userLogin : null;
        this.value = value ? value : null;
    }
}
