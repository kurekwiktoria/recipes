import { Measure } from 'app/core/model/measure.model';
import { Ingredient } from 'app/core/model/ingredient.model';

export interface IRecipeIngredient {
    id?: any;
    recipeId?: number;
    ingredientId?: number;
    ingredient?: Ingredient;
    measure?: Measure;
    quantity?: number;
    description?: string;
}

export class RecipeIngredient implements IRecipeIngredient {
    constructor(
        public id?: number,
        public recipeId?: number,
        public ingredientId?: number,
        public measure?: Measure,
        public quantity?: number,
        public description?: string,
        public ingredient?: Ingredient
    ) {
        this.id = id ? id : null;
        this.recipeId = recipeId ? recipeId : null;
        this.ingredientId = ingredientId ? ingredientId : null;
        this.measure = measure ? measure : null;
        this.quantity = quantity ? quantity : null;
        this.description = description ? description : null;
        this.ingredient = ingredient ? ingredient : null;
    }
}
