export interface ICategory {
    id?: any;
    name?: string;
    mainCategoryId?: any;
}

export class Category implements ICategory {
    constructor(public id?: any, public name?: string, public mainCategoryId?: any) {
        this.id = id ? id : null;
        this.name = name ? name : null;
        this.mainCategoryId = mainCategoryId ? mainCategoryId : null;
    }
}
