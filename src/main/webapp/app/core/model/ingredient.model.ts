export interface IIngredient {
    id?: any;
    name?: string;
    description?: string;
}

export class Ingredient implements IIngredient {
    constructor(public id?: any, public name?: string, public description?: string) {
        this.id = id ? id : null;
        this.name = name ? name : null;
        this.description = description ? description : null;
    }
}
