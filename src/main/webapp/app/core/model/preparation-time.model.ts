export interface IPreparationTime {
    id?: any;
    type?: string;
    quantity?: number;
}

export class PreparationTime implements IPreparationTime {
    constructor(public id?: any, public type?: string, public quantity?: number) {
        this.id = id ? id : null;
        this.type = type ? type : null;
        this.quantity = quantity ? quantity : null;
    }
}
