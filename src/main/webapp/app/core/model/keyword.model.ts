export interface IKeyword {
    id?: any;
    name?: string;
}

export class Keyword implements IKeyword {
    constructor(public id?: any, public name?: string) {
        this.id = id ? id : null;
        this.name = name ? name : null;
    }
}
