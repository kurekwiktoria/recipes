export interface IPreference {
    userId?: number;
    recipeId?: number;
    type?: string;
    description?: string;
}

export class Preference implements IPreference {
    constructor(public userId?: number, public recipeId?: number, public type?: string, public description?: string) {
        this.userId = userId ? userId : null;
        this.recipeId = recipeId ? recipeId : null;
        this.type = type ? type : null;
        this.description = description ? description : null;
    }
}
