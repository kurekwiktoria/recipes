import { Category } from 'app/core/model/category.model';
import { Keyword } from 'app/core/model/keyword.model';
import { RecipeIngredient } from 'app/core/model/recipe-ingredient.model';
import { RecipeStep } from 'app/core/model/recipe-step.model';
import { Rate } from 'app/core/model/rate.model';
import { RecipeSimilarity } from 'app/core/model/recipe-similarity.model';
import { Preference } from 'app/core/model/preference.model';
import { Comment } from 'app/core/model/comment.model';

export interface IRecipe {
    id?: any;
    title?: string;
    userLogin?: string;
    portions?: number;
    preparationTime?: any;
    difficultyLevel?: string;
    expense?: string;
    photoPath?: any;
    videoPath?: string;
    categories?: Category[];
    keywords?: Keyword[];
    recipeIngredients?: RecipeIngredient[];
    steps?: RecipeStep[];
    comments?: Comment[];
    rate?: number;
    newRate?: Rate;
    recipesSimilarity?: RecipeSimilarity[];
    preferences?: Preference[];
}

export class Recipe implements IRecipe {
    constructor(
        public id?: any,
        public title?: string,
        public userLogin?: string,
        public portions?: number,
        public preparationTime?: any,
        public difficultyLevel?: string,
        public expense?: string,
        public photoPath?: any,
        public videoPath?: string,
        public categories?: Category[],
        public keywords?: Keyword[],
        public recipeIngredients?: RecipeIngredient[],
        public steps?: RecipeStep[],
        public comments?: Comment[],
        public rate?: number,
        public recipesSimilarity?: RecipeSimilarity[],
        public preferences?: Preference[],
        public newRate?: Rate
    ) {
        this.id = id ? id : null;
        this.title = title ? title : null;
        this.userLogin = userLogin ? userLogin : null;
        this.portions = portions ? portions : null;
        this.preparationTime = preparationTime ? preparationTime : null;
        this.difficultyLevel = difficultyLevel ? difficultyLevel : null;
        this.expense = expense ? expense : null;
        this.photoPath = photoPath ? photoPath : null;
        this.videoPath = videoPath ? videoPath : null;
        this.categories = categories ? categories : null;
        this.keywords = keywords ? keywords : null;
        this.recipeIngredients = recipeIngredients ? recipeIngredients : null;
        this.steps = steps ? steps : null;
        this.comments = comments ? comments : null;
        this.rate = rate ? rate : null;
        this.recipesSimilarity = recipesSimilarity ? recipesSimilarity : null;
        this.preferences = preferences ? preferences : null;
        this.newRate = newRate ? newRate : null;
    }
}
