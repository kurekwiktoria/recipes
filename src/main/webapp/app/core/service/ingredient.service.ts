import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IIngredient } from 'app/core/model/ingredient.model';

@Injectable({ providedIn: 'root' })
export class IngredientService {
    public resourceUrl = SERVER_API_URL + 'api/ingredients';

    constructor(private http: HttpClient) {}

    create(ingredient: IIngredient): Observable<HttpResponse<IIngredient>> {
        return this.http.post<IIngredient>(this.resourceUrl, ingredient, { observe: 'response' });
    }

    update(ingredient: IIngredient): Observable<HttpResponse<IIngredient>> {
        return this.http.put<IIngredient>(this.resourceUrl, ingredient, { observe: 'response' });
    }

    find(name: string): Observable<HttpResponse<IIngredient>> {
        return this.http.get<IIngredient>(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }

    getAll(): Observable<HttpResponse<IIngredient[]>> {
        return this.http.get<IIngredient[]>(this.resourceUrl, { observe: 'response' });
    }

    delete(name: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }
}
