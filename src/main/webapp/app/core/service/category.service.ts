import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { ICategory } from 'app/core/model/category.model';

@Injectable({ providedIn: 'root' })
export class CategoryService {
    public resourceUrl = SERVER_API_URL + 'api/categories';

    constructor(private http: HttpClient) {}

    find(name: string): Observable<HttpResponse<ICategory>> {
        return this.http.get<ICategory>(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }

    getAll(): Observable<HttpResponse<ICategory[]>> {
        return this.http.get<ICategory[]>(this.resourceUrl, { observe: 'response' });
    }
}
