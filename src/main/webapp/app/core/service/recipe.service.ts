import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IRecipe } from 'app/core/model/recipe.model';
import { IRate } from 'app/core/model/rate.model';
import { IComment } from 'app/core/model/comment.model';

@Injectable({ providedIn: 'root' })
export class RecipeService {
    public resourceUrl = SERVER_API_URL + 'api/recipes';
    public resourceUrl1 = SERVER_API_URL + 'api/';

    constructor(private http: HttpClient) {}

    create(recipe: IRecipe): Observable<HttpResponse<IRecipe>> {
        return this.http.post<IRecipe>(this.resourceUrl, recipe, { observe: 'response' });
    }

    update(recipe: IRecipe): Observable<HttpResponse<IRecipe>> {
        return this.http.put<IRecipe>(this.resourceUrl, recipe, { observe: 'response' });
    }

    getAll(): Observable<HttpResponse<IRecipe[]>> {
        return this.http.get<IRecipe[]>(this.resourceUrl, { observe: 'response' });
    }

    getRecipe(id: any): Observable<HttpResponse<IRecipe>> {
        return this.http.get<IRecipe>(`${this.resourceUrl}/details/${id}`, { observe: 'response' });
    }

    findByCategory(category: string): Observable<HttpResponse<IRecipe[]>> {
        return this.http.get<IRecipe[]>(`${this.resourceUrl}/${category}`, { observe: 'response' });
    }

    getNewRecipes(): Observable<HttpResponse<IRecipe[]>> {
        return this.http.get<IRecipe[]>('api/newRecipes', { observe: 'response' });
    }

    rate(rate: IRate): Observable<HttpResponse<number>> {
        return this.http.post<number>(this.resourceUrl + '/rate', rate, { observe: 'response' });
    }

    search(parameter: string): Observable<HttpResponse<IRecipe[]>> {
        return this.http.get<IRecipe[]>(`${this.resourceUrl}/search/${parameter}`, { observe: 'response' });
    }

    comment(comment: IComment): Observable<HttpResponse<IComment>> {
        return this.http.post<IComment>(this.resourceUrl + '/comment', comment, { observe: 'response' });
    }

    removeComment(comment: IComment): Observable<HttpResponse<IComment>> {
        return this.http.put<IComment>(this.resourceUrl + '/comment', comment, { observe: 'response' });
    }

    removeRecipe(id: any): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getRecommendations(userLogin: string): Observable<HttpResponse<IRecipe[]>> {
        return this.http.get<IRecipe[]>(`${this.resourceUrl1}/recommendations/${userLogin}`, { observe: 'response' });
    }
}
