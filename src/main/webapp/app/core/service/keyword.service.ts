import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IKeyword } from 'app/core/model/keyword.model';

@Injectable({ providedIn: 'root' })
export class KeywordService {
    public resourceUrl = SERVER_API_URL + 'api/keywords';

    constructor(private http: HttpClient) {}

    create(keyword: IKeyword): Observable<HttpResponse<IKeyword>> {
        return this.http.post<IKeyword>(this.resourceUrl, keyword, { observe: 'response' });
    }

    update(keyword: IKeyword): Observable<HttpResponse<IKeyword>> {
        return this.http.put<IKeyword>(this.resourceUrl, keyword, { observe: 'response' });
    }

    find(name: string): Observable<HttpResponse<IKeyword>> {
        return this.http.get<IKeyword>(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }

    getAll(): Observable<HttpResponse<IKeyword[]>> {
        return this.http.get<IKeyword[]>(this.resourceUrl, { observe: 'response' });
    }

    delete(name: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }
}
