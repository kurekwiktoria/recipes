import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IFavourite } from 'app/core/model/favourite.model';

@Injectable({ providedIn: 'root' })
export class FavouritesService {
    public resourceUrl = SERVER_API_URL + 'api/favourites';
    public resource1rl = SERVER_API_URL + 'api/find';

    constructor(private http: HttpClient) {}

    create(favourite: IFavourite): Observable<HttpResponse<IFavourite>> {
        return this.http.post<IFavourite>(this.resourceUrl, favourite, { observe: 'response' });
    }

    update(recipe: IFavourite): Observable<HttpResponse<IFavourite>> {
        return this.http.put<IFavourite>(this.resourceUrl, recipe, { observe: 'response' });
    }

    getFavourite(id: any): Observable<HttpResponse<IFavourite>> {
        return this.http.get<IFavourite>(`${this.resourceUrl}/details/${id}`, { observe: 'response' });
    }

    findByUserLogin(userLogin: string): Observable<HttpResponse<IFavourite[]>> {
        return this.http.get<IFavourite[]>(`${this.resourceUrl}/${userLogin}`, { observe: 'response' });
    }

    delete(id: any): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    removeRecipeFromFavourite(recipeId: any, favouriteId: any) {
        return this.http.delete(`${this.resourceUrl}/${recipeId}/${favouriteId}`, { observe: 'response' });
    }

    addRecipeToFavourite(recipeId: any, favouriteId: any) {
        return this.http.get<IFavourite>(`${this.resourceUrl}/${recipeId}/${favouriteId}`, { observe: 'response' });
    }

    findByUserLoginAndNotRecipe(userLogin: string, recipeId: any): Observable<HttpResponse<IFavourite[]>> {
        return this.http.get<IFavourite[]>(`${this.resource1rl}/${userLogin}/${recipeId}`, { observe: 'response' });
    }

    checkUserLikedRecipe(userLogin: string, recipeId: any): Observable<HttpResponse<boolean>> {
        return this.http.get<boolean>(`${this.resourceUrl}/check/${userLogin}/${recipeId}`, { observe: 'response' });
    }
}
