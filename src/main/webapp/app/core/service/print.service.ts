import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from 'app/core/model/recipe.model';
import * as pdfMake from 'pdfmake/build/pdfmake';

import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { SERVER_API_URL } from 'app/app.constants';
import { RecipeIngredient } from 'app/core/model/recipe-ingredient.model';
import { RecipeStep } from 'app/core/model/recipe-step.model';

@Injectable({ providedIn: 'root' })
export class PrintService {
    downloadUrl = SERVER_API_URL + '/api/downloadFile/?path=';
    constructor(private http: HttpClient) {}

    generatePdf(action = 'open', recipe: Recipe) {
        const documentDefinition = this.getDocumentDefinition(recipe);
        // @ts-ignore
        pdfMake.vfs = pdfFonts.pdfMake.vfs;

        switch (action) {
            case 'open':
                // @ts-ignore
                pdfMake.createPdf(documentDefinition).open();
                break;
            case 'print':
                // @ts-ignore
                pdfMake.createPdf(documentDefinition).print();
                break;
            case 'download':
                // @ts-ignore
                pdfMake.createPdf(documentDefinition).download();
                break;

            default:
                // @ts-ignore
                pdfMake.createPdf(documentDefinition).open();
                break;
        }
    }

    getDocumentDefinition(recipe: Recipe) {
        let dd = {
            // Here you put the page settings as your footer, for example:
            footer: function(currentPage, pageCount) {
                return [
                    {
                        text: 'Przepysznik 2019',
                        alignment: currentPage ? 'center' : 'center'
                    }
                ];
            },
            // Here you can enter the page size and orientation:
            pageSize: 'A4',
            pageOrientation: 'Portrait',
            //in pageOrientation you can put "Portrait" or "landscape"

            // start the body of your impression:
            content: [
                {
                    text: recipe.title,
                    bold: true,
                    fontSize: 25,
                    alignment: 'center',
                    margin: [0, 0, 0, 10]
                },
                {
                    columns: [
                        [
                            {
                                text: 'Autor: ' + recipe.userLogin,
                                fontSize: 14,
                                alignment: 'center'
                            }
                        ]
                    ]
                },
                {
                    columns: [
                        [
                            {
                                width: 50,
                                alignment: 'center',
                                text: recipe.preparationTime.quantity + ' min.'
                            }
                        ],
                        [
                            {
                                width: 50,
                                alignment: 'center',
                                text:
                                    recipe.portions +
                                    (recipe.portions === 1
                                        ? ' porcja'
                                        : recipe.portions >= 2 && recipe.portions < 5
                                        ? ' porcje'
                                        : ' porcji')
                            }
                        ]
                    ]
                },
                {
                    text: 'Składniki',
                    bold: true,
                    italic: true,
                    fontSize: 14,
                    margin: [15, 15, 15, 15]
                },
                this.getRecipeIngredients(recipe.recipeIngredients),
                this.getRecipeSteps(recipe.steps)
            ]
        };

        return dd;
        // @ts-ignore
        // pdfMake.createPdf(dd).download(recipe.title);
    }

    getRecipeIngredients(recipeIngredients: RecipeIngredient[]) {
        const exs = [];
        recipeIngredients.forEach(recipeIngredient => {
            exs.push([
                {
                    columns: [
                        [
                            {
                                text: recipeIngredient.ingredient.name,
                                bold: true,
                                alignment: 'center',
                                margin: [5, 5, 5, 5]
                            }
                        ],
                        [
                            {
                                text: recipeIngredient.quantity,
                                alignment: 'right',
                                margin: [5, 5, 5, 5]
                            }
                        ],
                        [
                            {
                                text: recipeIngredient.measure.type,
                                alignment: 'left',
                                margin: [5, 5, 5, 5]
                            }
                        ]
                    ]
                }
            ]);
        });
        return {
            table: {
                widths: ['*'],
                body: [...exs]
            }
        };
    }

    getRecipeSteps(recipeSteps: RecipeStep[]) {
        const exs = [];
        let index = 0;
        recipeSteps.forEach(recipeStep => {
            index++;
            exs.push([
                {
                    columns: [
                        [
                            {
                                text: 'Krok ' + index,
                                bold: true,
                                fontSize: 20
                            },
                            {
                                text: recipeStep.description,
                                margin: [5, 5, 5, 5]
                            }
                        ]
                    ]
                }
            ]);
        });
        return {
            table: {
                widths: ['*'],
                body: [...exs]
            },
            layout: 'noBorders'
        };
    }
}
