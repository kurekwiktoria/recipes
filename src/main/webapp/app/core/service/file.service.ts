import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';

@Injectable({ providedIn: 'root' })
export class FileService {
    public resourceUrl = SERVER_API_URL + 'api/';

    constructor(private http: HttpClient) {}

    save(data: FormData) {
        return this.http.post(this.resourceUrl + 'saveFile', data, { responseType: 'text' });
    }

    // download(path: string): Observable<HttpResponse<File>> {
    //     return this.http.get<File>(`${this.resourceUrl + 'downloadFile'}/?path=${path}`, {
    //         responseType: 'image/jpeg',
    //         observe: 'response'
    //     });
    // }
}
