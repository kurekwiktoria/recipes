import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IMeasure } from 'app/core/model/measure.model';

@Injectable({ providedIn: 'root' })
export class MeasureService {
    public resourceUrl = SERVER_API_URL + 'api/measures';

    constructor(private http: HttpClient) {}

    create(measure: IMeasure): Observable<HttpResponse<IMeasure>> {
        return this.http.post<IMeasure>(this.resourceUrl, measure, { observe: 'response' });
    }

    update(measure: IMeasure): Observable<HttpResponse<IMeasure>> {
        return this.http.put<IMeasure>(this.resourceUrl, measure, { observe: 'response' });
    }

    find(name: string): Observable<HttpResponse<IMeasure>> {
        return this.http.get<IMeasure>(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }

    getAll(): Observable<HttpResponse<IMeasure[]>> {
        return this.http.get<IMeasure[]>(this.resourceUrl, { observe: 'response' });
    }

    delete(name: string): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${name}`, { observe: 'response' });
    }
}
