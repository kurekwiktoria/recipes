import { LOCALE_ID, NgModule } from '@angular/core';
import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import locale from '@angular/common/locales/pl';
import { AccountService } from 'app/core/auth/account.service';

@NgModule({
    imports: [HttpClientModule],
    exports: [],
    declarations: [],
    providers: [
        Title,
        {
            provide: LOCALE_ID,
            useValue: 'pl'
        },
        DatePipe,
        AccountService
    ]
})
export class RecipesCoreModule {
    constructor() {
        registerLocaleData(locale);
    }
}
