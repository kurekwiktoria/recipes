import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Account, AccountService, LoginModalService } from 'app/core';
import { RecipeService } from 'app/core/service/recipe.service';
import { Recipe } from 'app/core/model/recipe.model';
import { FileService } from 'app/core/service/file.service';
import { SERVER_API_URL } from 'app/app.constants';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    recipes: Recipe[];
    file;
    downloadUrl = SERVER_API_URL + '/api/downloadFile/?path=';
    images = [1, 2, 3].map(() => `https://picsum.photos/2048/500?random&t=${Math.random()}`);

    constructor(
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private recipeService: RecipeService,
        private fileService: FileService
    ) {}

    ngOnInit() {
        this.accountService.identity().then((account: Account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
        this.getNewRecipes();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.accountService.identity().then(account => {
                this.account = account;
            });
        });
    }

    getNewRecipes() {
        this.recipeService.getNewRecipes().subscribe(recipes => {
            this.recipes = recipes.body;
        });
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
}
