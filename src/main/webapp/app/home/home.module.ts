import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RecipesSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [RecipesSharedModule, NgbModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecipesHomeModule {}
