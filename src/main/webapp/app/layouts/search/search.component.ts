import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'app/core/service/recipe.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'jhi-search',
    templateUrl: './search.component.html',
    styles: []
})
export class SearchComponent implements OnInit {
    parameters: string = '';
    constructor(private recipeService: RecipeService, private route: ActivatedRoute, private router: Router) {}

    ngOnInit() {}

    search() {
        if (this.parameters !== '') {
            this.router.navigate(['../przepisy/szukaj/' + this.parameters], { relativeTo: this.route.parent });
            this.parameters = '';
        }
    }
}
