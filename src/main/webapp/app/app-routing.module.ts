import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute, navbarRoute } from './layouts';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
    imports: [
        RouterModule.forRoot(
            [
                {
                    path: 'admin',
                    loadChildren: './admin/admin.module#RecipesAdminModule'
                },
                {
                    path: 'przepisy',
                    loadChildren: './recipes/recipes.module#RecipesModule'
                },
                {
                    path: 'ulubione',
                    loadChildren: './favourites/favourites.module#FavouritesModule'
                },
                {
                    path: 'rekomendacje',
                    loadChildren: './recommendations/recommendations.module#RecommendationsModule'
                },

                ...LAYOUT_ROUTES
            ],
            { useHash: true, enableTracing: DEBUG_INFO_ENABLED }
        )
    ],
    exports: [RouterModule]
})
export class RecipesAppRoutingModule {}
