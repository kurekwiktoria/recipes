/* after changing this file run 'npm run webpack:build' */
/* tslint:disable */
import '../content/scss/vendor.scss';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faAppleAlt,
    faArrowLeft,
    faAsterisk,
    faBalanceScale,
    faBan,
    faBars,
    faBell,
    faBook,
    faBookmark,
    faBreadSlice,
    faBurn,
    faCalendarAlt,
    faCarrot,
    faClipboardList,
    faClock,
    faCloud,
    faCogs,
    faComments,
    faCookieBite,
    faDrumstickBite,
    faEye,
    faFlag,
    faHdd,
    faHeart,
    faHome,
    faKey,
    faLeaf,
    faLightbulb,
    faList,
    faMoneyBill,
    faMugHot,
    faPencilAlt,
    faPepperHot,
    faPizzaSlice,
    faPlus,
    faReply,
    faRoad,
    faSave,
    faSearch,
    faSignInAlt,
    faSignOutAlt,
    faSort,
    faSortDown,
    faSortUp,
    faSync,
    faTachometerAlt,
    faTag,
    faTasks,
    faThermometerEmpty,
    faThermometerFull,
    faThermometerHalf,
    faThList,
    faTimes,
    faTrash,
    faTrashAlt,
    faUser,
    faUserPlus,
    faWrench
} from '@fortawesome/free-solid-svg-icons';
import { faCocktail } from '@fortawesome/free-solid-svg-icons/faCocktail';
import { faPlusSquare } from '@fortawesome/free-solid-svg-icons/faPlusSquare';
import { faImage } from '@fortawesome/free-solid-svg-icons/faImage';

// Imports all fontawesome core and solid icons

// Adds the SVG icon to the library so you can use it in your page
library.add(faUser);
library.add(faSort);
library.add(faSortUp);
library.add(faSortDown);
library.add(faSync);
library.add(faEye);
library.add(faBan);
library.add(faClipboardList);
library.add(faLightbulb);
library.add(faTimes);
library.add(faArrowLeft);
library.add(faThermometerEmpty);
library.add(faThermometerFull);
library.add(faThermometerHalf);
library.add(faSave);
library.add(faSearch);
library.add(faPlus);
library.add(faPencilAlt);
library.add(faBars);
library.add(faHome);
library.add(faThList);
library.add(faUserPlus);
library.add(faRoad);
library.add(faTachometerAlt);
library.add(faHeart);
library.add(faList);
library.add(faBell);
library.add(faTasks);
library.add(faBook);
library.add(faHdd);
library.add(faFlag);
library.add(faWrench);
library.add(faClock);
library.add(faCloud);
library.add(faSignOutAlt);
library.add(faSignInAlt);
library.add(faCalendarAlt);
library.add(faSearch);
library.add(faTrashAlt);
library.add(faAsterisk);
library.add(faAppleAlt);
library.add(faCarrot);
library.add(faCocktail);
library.add(faMugHot);
library.add(faPepperHot);
library.add(faLeaf);
library.add(faComments);
library.add(faBurn);
library.add(faDrumstickBite);
library.add(faPizzaSlice);
library.add(faBreadSlice);
library.add(faCookieBite);
library.add(faCogs);
library.add(faKey);
library.add(faTag);
library.add(faBalanceScale);
library.add(faBookmark);
library.add(faTrash);
library.add(faBook);
library.add(faPlusSquare);
library.add(faTrash);
library.add(faImage);
library.add(faMoneyBill);
library.add(faReply);

// jhipster-needle-add-element-to-vendor - JHipster will add new menu items here
