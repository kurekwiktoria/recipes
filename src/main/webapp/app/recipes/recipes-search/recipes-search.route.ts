import { Route } from '@angular/router';
import { RecipesSearchComponent } from 'app/recipes/recipes-search/recipes-search.component';

export const recipesSearchRoute: Route = {
    path: 'szukaj/:param',
    component: RecipesSearchComponent,
    data: {
        pageTitle: 'recipes.search.title'
    }
};
