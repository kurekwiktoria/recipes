import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from 'app/core/service/recipe.service';
import { Recipe } from 'app/core/model/recipe.model';
import { SERVER_API_URL } from 'app/app.constants';

@Component({
    selector: 'jhi-recipes-search',
    templateUrl: './recipes-search.component.html',
    styles: []
})
export class RecipesSearchComponent implements OnInit {
    title: 'Lista przepisów';
    recipes: Recipe[];
    downloadUrl = SERVER_API_URL + '/api/downloadFile/?path=';
    params: string;

    constructor(private route: ActivatedRoute, private router: Router, private recipeService: RecipeService) {}

    ngOnInit() {
        this.route.params.subscribe(routeParams => {
            this.params = routeParams.param;
            this.search();
        });
    }

    goToDetails(id: any) {
        this.router.navigate(['../przepisy/szczegoly/' + id], { relativeTo: this.route.parent });
    }

    search() {
        this.recipeService.search(this.params).subscribe(recipes => {
            this.recipes = recipes.body;
        });
    }
}
