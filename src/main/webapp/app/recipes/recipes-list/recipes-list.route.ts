import { Route } from '@angular/router';

import { RecipesListComponent } from 'app/recipes/recipes-list/recipes-list.component';

export const recipesListRoute: Route = {
    path: ':category',
    component: RecipesListComponent,
    data: {
        pageTitle: 'recipes.list.title'
    }
};
