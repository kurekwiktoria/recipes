import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from 'app/core/service/recipe.service';
import { Recipe } from 'app/core/model/recipe.model';
import { SERVER_API_URL } from 'app/app.constants';

@Component({
    selector: 'jhi-recipes-list',
    templateUrl: './recipes-list.component.html',
    styles: []
})
export class RecipesListComponent implements OnInit {
    category: string;
    title: string;
    recipes: Recipe[];
    downloadUrl = SERVER_API_URL + '/api/downloadFile/?path=';

    @Input() searchParams: string;
    constructor(private route: ActivatedRoute, private router: Router, private recipeService: RecipeService) {}

    ngOnInit() {
        this.route.params.subscribe(routeParams => {
            console.log(routeParams);
            this.category = routeParams.category;
            this.getTitle(routeParams.category);
            if (this.title === 'Lista przepisów') {
                this.search();
            } else {
                this.getRecipes();
            }
        });
    }

    getRecipes() {
        this.recipeService.findByCategory(this.category).subscribe(recipes => {
            this.recipes = recipes.body;
        });
    }

    getTitle(category: string) {
        switch (category) {
            case 'sniadania':
                this.title = 'Śniadania';
                break;
            case 'lunche':
                this.title = 'Lunche';
                break;
            case 'zupy':
                this.title = 'Zupy';
                break;
            case 'salatki':
                this.title = 'Sałatki';
                break;
            case 'surowki':
                this.title = 'Surówki';
                break;
            case 'makarony':
                this.title = 'Makarony';
                break;
            case 'dania-glowne':
                this.title = ' Dania główne';
                break;
            case 'podwieczorki':
                this.title = 'Podwieczorki';
                break;
            case 'napoje':
                this.title = 'Napoje';
                break;
            case 'kolacje':
                this.title = 'Kolacje';
                break;
            case 'desery':
                this.title = 'Desery';
                break;
            default:
                this.title = 'Lista przepisów';
                break;
        }
    }

    goToDetails(id: any) {
        this.router.navigate(['../przepisy/szczegoly/' + id], { relativeTo: this.route.parent });
    }

    search() {
        this.recipeService.search(this.searchParams).subscribe(recipes => {
            this.recipes = recipes.body;
        });
    }
}
