import { Route } from '@angular/router';
import { RecipesDetailsComponent } from 'app/recipes/recipes-details/recipes-details.component';

export const recipesDetailsRoute: Route = {
    path: 'szczegoly/:id',
    component: RecipesDetailsComponent,
    data: {
        pageTitle: 'recipes.details.title'
    }
};
