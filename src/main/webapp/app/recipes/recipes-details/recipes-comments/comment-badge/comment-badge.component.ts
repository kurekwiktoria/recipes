import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-comment-badge',
    templateUrl: './comment-badge.component.html',
    styles: []
})
export class CommentBadgeComponent implements OnInit {
    @Input() type: string;
    constructor() {}

    ngOnInit() {}
}
