import { Component, Input, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Comment } from 'app/core/model/comment.model';
import { RecipeService } from 'app/core/service/recipe.service';
import { Recipe } from 'app/core/model/recipe.model';
import { Account, AccountService } from 'app/core';

@Component({
    selector: 'jhi-recipes-comments',
    templateUrl: './recipes-comments.component.html',
    styles: [],
    styleUrls: ['./recipes-comments.scss']
})
export class RecipesCommentsComponent implements OnInit {
    @Input() recipe: Recipe;
    commentType = 'PUBLIC';
    commentText: string;
    replyText: string;
    replyType = 'PUBLIC';
    account: Account;
    replyCommentId: any;
    filterType = 'ALL';

    constructor(private recipeService: RecipeService, private accountService: AccountService) {}

    ngOnInit() {
        this.accountService.identity().then((account: Account) => {
            this.account = account;
        });
    }

    addComment() {
        this.recipeService
            .comment(new Comment(null, this.account.login, this.recipe.id, this.commentType, this.commentText, null, null))
            .subscribe(result => {
                let c = new Comment(result.body.id, this.account.login, this.recipe.id, result.body.type, result.body.description);
                this.recipe.comments.push(c);
            });
        this.commentText = '';
    }

    remove(comment: Comment) {
        this.recipeService.removeComment(comment).subscribe(result => {
            if (this.recipe.comments.indexOf(comment) > -1) {
                this.recipe.comments[this.recipe.comments.indexOf(comment)].description = 'Komentarz został usunięty przez autora.';
            } else {
                if (this.recipe.comments.findIndex(c => c.id === comment.commentId) > -1) {
                    let index = this.recipe.comments.findIndex(c => c.id === comment.commentId);
                    let replyIndex = this.recipe.comments[index].replies.findIndex(r => r.id === comment.id);
                    this.recipe.comments[index].replies[replyIndex].description = 'Komentarz został usunięty przez autora.';
                }
            }
        });
    }

    replyClicked(replyCommentId: any) {
        this.replyCommentId = replyCommentId;
    }

    saveReply() {
        this.recipeService
            .comment(new Comment(null, this.account.login, this.recipe.id, this.replyType, this.replyText, this.replyCommentId, null))
            .subscribe(result => {
                let c = new Comment(
                    result.body.id,
                    this.account.login,
                    this.recipe.id,
                    result.body.type,
                    result.body.description,
                    this.replyCommentId
                );
                this.recipe.comments[this.recipe.comments.findIndex(comment => comment.id === this.replyCommentId)].replies.push(c);
                this.replyCommentId = null;
            });
        this.replyText = '';
    }
}

@Pipe({
    name: 'filterPrivateComments',
    pure: false
})
export class FilterPrivateComments implements PipeTransform {
    transform(items: any[], login: string): any {
        if (!items || !login) {
            return items;
        }
        return items.filter(comment => comment.type !== 'PRIVATE' || (comment.type === 'PRIVATE' && comment.userLogin === login));
    }
}

@Pipe({
    name: 'filterCommentsByType',
    pure: false
})
export class FilterCommentsByType implements PipeTransform {
    transform(items: any[], type: string): any {
        if (!items || !type || type === 'ALL') {
            return items;
        }
        return items.filter(comment => comment.type === type);
    }
}
