import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FavouritesService } from 'app/core/service/favourites.service';
import { Favourite } from 'app/core/model/favourite.model';
import { ActivatedRoute, Router } from '@angular/router';

class DialogData {}

@Component({
    selector: 'jhi-recipes-favourites-dialog',
    templateUrl: 'recipes-favourites-dialog.component.html'
})
export class RecipesFavouritesDialogComponent implements OnInit {
    favourites: Favourite[];
    selectedFav: Favourite;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public dialogRef: MatDialogRef<RecipesFavouritesDialogComponent>,
        private favouritesService: FavouritesService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {}

    ngOnInit(): void {
        // @ts-ignore
        this.favouritesService.findByUserLoginAndNotRecipe(this.data.userLogin, this.data.recipeId).subscribe(favs => {
            this.favourites = favs.body;
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    addToList() {
        // @ts-ignore
        this.favouritesService.addRecipeToFavourite(this.data.recipeId, this.selectedFav).subscribe();
    }

    goToNewList() {
        this.router.navigate(['../ulubione/listy'], { relativeTo: this.route.parent });
    }
}
