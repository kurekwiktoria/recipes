import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from 'app/core/service/recipe.service';
import { Recipe } from 'app/core/model/recipe.model';
import { StarRatingComponent } from 'ng-starrating';
import { Rate } from 'app/core/model/rate.model';
import { Account, AccountService } from 'app/core';
import { SERVER_API_URL } from 'app/app.constants';
import { FavouritesService } from 'app/core/service/favourites.service';
import { Favourite } from 'app/core/model/favourite.model';
import { combineLatest } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { RecipesFavouritesDialogComponent } from 'app/recipes/recipes-details/recipes-favourites-dialog/recipes-favourites-dialog.component';
import { PrintService } from 'app/core/service/print.service';

@Component({
    selector: 'jhi-recipes-details',
    templateUrl: './recipes-details.component.html',
    styles: [],
    styleUrls: ['./recipes-details.scss']
})
export class RecipesDetailsComponent implements OnInit {
    id: any;
    recipe: Recipe;
    account: Account;
    downloadUrl = SERVER_API_URL + '/api/downloadFile/?path=';
    favourites: Favourite[];
    selectedFav: number;
    userFavourites: Favourite[];
    recipeLiked = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private recipeService: RecipeService,
        private accountService: AccountService,
        private favouritesService: FavouritesService,
        public dialog: MatDialog,
        public printService: PrintService
    ) {}

    ngOnInit() {
        const recipe$ = this.route.params;
        const account$ = this.accountService.identity();

        combineLatest(recipe$, account$, (recipe, account) => ({ recipe, account })).subscribe(pair => {
            this.id = pair.recipe.id;
            this.account = pair.account;
            this.getRecipe();
            this.checkIsLiked();
        });
    }

    getRecipe() {
        this.recipeService.getRecipe(this.id).subscribe(recipes => {
            this.recipe = recipes.body;
        });
    }

    onRate($event: { oldValue: number; newValue: number; starRating: StarRatingComponent }) {
        this.recipeService.rate(new Rate(this.recipe.id, this.account.login, null, null, $event.newValue)).subscribe(rate => {
            this.recipe.rate = rate.body;
        });
    }

    search(param: string) {
        this.router.navigate(['../przepisy/szukaj/' + param], { relativeTo: this.route.parent });
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(RecipesFavouritesDialogComponent, {
            width: '300px',
            data: { recipeId: this.recipe.id, userLogin: this.account.login }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.recipeLiked = result;
        });
    }

    checkIsLiked() {
        this.favouritesService.checkUserLikedRecipe(this.account.login, this.id).subscribe(res => {
            this.recipeLiked = res.body;
        });
    }

    generatePdf(action = 'open') {
        this.printService.generatePdf(action, this.recipe);
    }

    remove() {
        this.recipeService.removeRecipe(this.recipe.id).subscribe(() => {
            this.router.navigate(['../'], { relativeTo: this.route.parent });
        });
    }
}

@Pipe({
    name: 'filterCommentsWithoutReplies',
    pure: false
})
export class FilterCommentsWithoutReplies implements PipeTransform {
    transform(items: any[]): any {
        if (!items) {
            return items;
        }
        return items.filter(comment => comment.commentId === null);
    }
}
