import { Component, OnDestroy, OnInit, Pipe, PipeTransform, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Category } from 'app/core/model/category.model';
import { CategoryService } from 'app/core/service/category.service';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material';
import { Options } from 'ng5-slider';
import { Ingredient } from 'app/core/model/ingredient.model';
import { IngredientService } from 'app/core/service/ingredient.service';
import { Measure } from 'app/core/model/measure.model';
import { MeasureService } from 'app/core/service/measure.service';
import { RecipeIngredient } from 'app/core/model/recipe-ingredient.model';
import { Recipe } from 'app/core/model/recipe.model';
import { RecipeStep } from 'app/core/model/recipe-step.model';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Account, AccountService } from 'app/core';
import { FileService } from 'app/core/service/file.service';
import { RecipeService } from 'app/core/service/recipe.service';
import { COMMA, ENTER, SEMICOLON } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/typings/chips';
import { PreparationTime } from 'app/core/model/preparation-time.model';
import { Keyword } from 'app/core/model/keyword.model';
import { Router } from '@angular/router';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) {}
    transform(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}

@Component({
    selector: 'jhi-recipes-add',
    templateUrl: './recipes-add.component.html',
    styles: [],
    styleUrls: ['./recipes-add.scss']
})
export class RecipesAddComponent implements OnInit, OnDestroy {
    title = '';
    account: Account;
    generalInfo: FormGroup;
    photo = 'nie';
    video = 'nie';
    ingredientsGroup: FormGroup;
    categories: Category[];
    category: Category;
    ingredients: Ingredient[];
    ingredient: Ingredient;
    measures: Measure[];
    difficultyLevel = 2;
    preparationTime: number;
    difficultyLevelEnum: Options = {
        floor: 1,
        ceil: 6,
        showTicks: true,
        stepsArray: [{ value: 1, legend: 'Łatwe' }, { value: 2, legend: 'Średnie' }, { value: 3, legend: 'Trudne' }]
    };
    priceLevel = 2;
    priceLevelEnum: Options = {
        floor: 1,
        ceil: 6,
        showTicks: true,
        stepsArray: [{ value: 1, legend: 'Tanie' }, { value: 2, legend: 'Niedrogie' }, { value: 3, legend: 'Drogie' }]
    };
    portions = 1;
    portionsEnum: Options = {
        floor: 1,
        ceil: 10,
        showTicks: true
    };
    index = 0;
    stepIndex = 0;
    recipeIngredients: RecipeIngredient[] = [];
    ingredientGroup = new FormGroup({});
    stepsGroup = new FormGroup({});
    recipeSteps: RecipeStep[] = [];
    fileData: File = null;
    previewUrl: any = null;
    videoLink: string;
    fileUploadProgress: string = null;
    photoPath: string = null;

    public categoryCtrl: FormControl = new FormControl();
    public categoryFilterCtrl: FormControl = new FormControl();
    public ingredientCtrl: FormControl = new FormControl();
    public ingredientFilterCtrl: FormControl = new FormControl();
    readonly separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
    public keywords: string[] = [];
    private _onDestroy = new Subject<void>();
    @ViewChild('singleSelect') singleSelect: MatSelect;

    public filteredCategories: ReplaySubject<Category[]> = new ReplaySubject<Category[]>(1);
    public filteredIngredients: ReplaySubject<Ingredient[]> = new ReplaySubject<Ingredient[]>(1);

    constructor(
        private _formBuilder: FormBuilder,
        private categoryService: CategoryService,
        private ingredientService: IngredientService,
        private measureService: MeasureService,
        private http: HttpClient,
        private _sanitizer: DomSanitizer,
        private accountService: AccountService,
        private fileService: FileService,
        public recipeService: RecipeService,
        private route: Router
    ) {}

    ngOnInit() {
        this.index = 1;
        this.stepIndex = 1;
        this.generalInfo = this._formBuilder.group({
            name: ['', Validators.required],
            category: ['', Validators.required],
            time: ['', Validators.required]
        });
        this.categoryService.getAll().subscribe(categories => {
            this.categories = categories.body;
            this.setInitialCategories();
            this.categoryCtrl.setValue(this.categories);
            this.filteredCategories.next(this.categories.slice());
            this.categoryFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
                this.filterCategories();
            });
        });
        this.ingredientService.getAll().subscribe(ing => {
            this.ingredients = ing.body;
            this.setInitialIngredients();
            this.ingredientCtrl.setValue(this.ingredients);
            this.filteredIngredients.next(this.ingredients.slice());
            this.ingredientFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
                this.filterIngredients();
            });
        });
        this.measureService.getAll().subscribe(measure => {
            this.measures = measure.body;
        });
        this.accountService.identity().then((account: Account) => {
            this.account = account;
        });
        this.addIngredient();
        this.addStep();
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    private filterCategories() {
        if (!this.categories) {
            return;
        }
        let search = this.categoryFilterCtrl.value;
        if (!search) {
            this.filteredCategories.next(this.categories.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        this.filteredCategories.next(this.categories.filter(c => c.name.toLowerCase().indexOf(search) > -1));
    }

    private filterIngredients() {
        if (!this.ingredients) {
            return;
        }
        let search = this.ingredientFilterCtrl.value;
        if (!search) {
            this.filteredIngredients.next(this.ingredients.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        this.filteredIngredients.next(this.ingredients.filter(i => i.name.toLowerCase().indexOf(search) > -1));
    }

    private setInitialCategories() {
        this.filteredCategories
            .pipe(
                take(1),
                takeUntil(this._onDestroy)
            )
            .subscribe(() => {
                this.singleSelect.compareWith = (a: Category, b: Category) => a.id === b.id;
            });
    }
    private setInitialIngredients() {
        this.filteredCategories
            .pipe(
                take(1),
                takeUntil(this._onDestroy)
            )
            .subscribe(() => {
                this.singleSelect.compareWith = (a: Category, b: Category) => a.id === b.id;
            });
    }

    addIngredient() {
        this.recipeIngredients.push(new RecipeIngredient(this.index));
        this.ingredientGroup.addControl('in' + this.index, new FormControl('', Validators.required));
        this.ingredientGroup.addControl('me' + this.index, new FormControl('', Validators.required));
        this.ingredientGroup.addControl('meVal' + this.index, new FormControl('', Validators.required));
        this.index++;
    }

    removeIngredient(container: any) {
        this.recipeIngredients = this.recipeIngredients.filter(item => item.id !== container);
        this.ingredientGroup.removeControl('in' + container);
        this.ingredientGroup.removeControl('me' + container);
        this.ingredientGroup.removeControl('meVal' + container);
    }

    onMeasureValueChange(value: string, recipeIngredient: RecipeIngredient) {
        recipeIngredient.quantity = parseFloat(value);
    }

    addStep() {
        this.recipeSteps.push(new RecipeStep(null, null, this.stepIndex));
        this.stepsGroup.addControl('step' + this.stepIndex, new FormControl('', Validators.required));
        this.stepIndex++;
    }

    removeStep(container: any) {
        this.recipeSteps = this.recipeSteps.filter(item => item.sortableNumber !== container);
        this.stepsGroup.removeControl('step' + container);
        this.stepIndex--;
    }

    onStepValueChange(value: string, recipeStep: RecipeStep) {
        recipeStep.description = value;
    }

    fileProgress(fileInput: any, recipeStep: RecipeStep) {
        recipeStep.fileData = <File>fileInput.target.files[0];
        const fdata: FormData = new FormData();
        fdata.append('file', recipeStep.fileData);
        this.fileService.save(fdata).subscribe(path => (recipeStep.photo = path));
        this.preview(recipeStep);
    }

    addPhoto(fileInput: any) {
        this.fileData = <File>fileInput.target.files[0];
        const mimeType = this.fileData.type;
        if (mimeType.match(/image\/*/) == null) {
            return;
        }

        const reader = new FileReader();
        reader.readAsDataURL(this.fileData);
        reader.onload = _event => {
            this.previewUrl = reader.result;
        };
        const formdata: FormData = new FormData();
        formdata.append('file', this.fileData);
        this.fileService.save(formdata).subscribe(path => (this.photoPath = path));
    }

    addVideoLink(videoLink: any) {
        const link = String(videoLink.target.value);
        let index = link.lastIndexOf('v=');
        if (index > 0) {
            this.videoLink = String('https://www.youtube.com/embed/' + link.slice(index + 2));
        } else {
            index = link.lastIndexOf('embed/');
            if (index > 0) {
                this.videoLink = link;
            } else {
                index = link.lastIndexOf('/');
                this.videoLink = String('https://www.youtube.com/embed/' + link.slice(index + 1));
            }
        }
    }

    preview(recipeStep: RecipeStep) {
        // Show preview
        const mimeType = recipeStep.fileData.type;
        if (mimeType.match(/image\/*/) == null) {
            return;
        }

        const reader = new FileReader();
        reader.readAsDataURL(recipeStep.fileData);
        reader.onload = _event => {
            recipeStep.previewUrl = reader.result;
        };
    }

    addKeyword(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            this.keywords.push(value.trim());
        }

        // Reset the input value
        if (input) {
            input.value = '';
        }
    }

    removeKeyword(keyword: string): void {
        const index = this.keywords.indexOf(keyword);

        if (index >= 0) {
            this.keywords.splice(index, 1);
        }
    }

    previousState() {
        console.log('Udalo sie zapisac!');
        this.route.navigateByUrl('/');
    }

    save() {
        this.accountService.identity().then(account => (this.account = account));
        const diff = this.difficultyLevel === 1 ? 'EASY' : this.difficultyLevel === 2 ? 'MEDIUM' : 'DIFFICULT';
        const expense = this.priceLevel === 1 ? 'CHEAP' : this.priceLevel === 2 ? 'MEDIUM' : 'EXPENSIVE';
        let ctgs: Category[];
        ctgs = [this.category];
        const prepTime: PreparationTime = new PreparationTime(null, 'MINUTES', this.preparationTime);

        const kwrds: Keyword[] = [];
        let i = 0;
        this.keywords.forEach(function(keyword) {
            kwrds[i] = new Keyword(null, keyword);
            i++;
        });
        const recipe = new Recipe(
            null,
            this.title,
            this.account.login,
            this.portions,
            prepTime,
            diff,
            expense,
            this.photoPath,
            this.videoLink,
            ctgs,
            kwrds,
            this.recipeIngredients,
            this.recipeSteps,
            null,
            null,
            null,
            null
        );

        this.recipeService.create(recipe).subscribe(
            response => {
                this.previousState();
            },
            () => console.log('Blad podczas zapisu')
        );
    }

    ingredientAdded(recipeIngredient, event) {
        this.ingredientService.getAll().subscribe(ing => {
            this.ingredients = ing.body;
            this.setInitialIngredients();
            this.ingredientCtrl.setValue(this.ingredients);
            this.filteredIngredients.next(this.ingredients.slice());
            this.ingredientFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
                this.filterIngredients();
            });
        });
        recipeIngredient.ingredient = event;
    }
}
