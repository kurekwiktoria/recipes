import { Route } from '@angular/router';

import { RecipesListComponent } from 'app/recipes/recipes-list/recipes-list.component';
import { RecipesAddComponent } from 'app/recipes/recipes-add/recipes-add.component';

export const recipesAddRoute: Route = {
    path: 'dodaj',
    component: RecipesAddComponent,
    data: {
        pageTitle: 'recipes.add.title'
    }
};
