import { Routes } from '@angular/router';
import { recipesListRoute } from 'app/recipes/recipes-list/recipes-list.route';
import { recipesAddRoute } from 'app/recipes/recipes-add/recipes-add.route';
import { recipesDetailsRoute } from 'app/recipes/recipes-details/recipes-details.route';
import { recipesSearchRoute } from 'app/recipes/recipes-search/recipes-search.route';

const RECIPES_ROUTE = [recipesSearchRoute, recipesAddRoute, recipesListRoute, recipesDetailsRoute];

export const recipesState: Routes = [
    {
        path: '',
        children: RECIPES_ROUTE
    }
];
