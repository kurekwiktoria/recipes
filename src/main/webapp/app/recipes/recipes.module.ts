import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesSharedModule } from 'app/shared';
import { recipesState } from 'app/recipes/recipes.route';
import { RouterModule } from '@angular/router';
import { RecipesListComponent } from './recipes-list/recipes-list.component';
import { RecipesAddComponent, SafePipe } from './recipes-add/recipes-add.component';
import {
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatStepperModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { Ng5SliderModule } from 'ng5-slider';
import { FilterCommentsWithoutReplies, RecipesDetailsComponent } from './recipes-details/recipes-details.component';
import { RatingModule } from 'ng-starrating';
import { RecipesSearchComponent } from './recipes-search/recipes-search.component';
import { MatSelectSearchModule } from 'app/shared/select-with-search/mat-select-search.module';
import {
    FilterCommentsByType,
    FilterPrivateComments,
    RecipesCommentsComponent
} from './recipes-details/recipes-comments/recipes-comments.component';
import { CommentBadgeComponent } from './recipes-details/recipes-comments/comment-badge/comment-badge.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { RecipesFavouritesDialogComponent } from 'app/recipes/recipes-details/recipes-favourites-dialog/recipes-favourites-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
    declarations: [
        RecipesListComponent,
        RecipesAddComponent,
        SafePipe,
        RecipesDetailsComponent,
        RecipesSearchComponent,
        RecipesCommentsComponent,
        CommentBadgeComponent,
        FilterPrivateComments,
        FilterCommentsWithoutReplies,
        RecipesFavouritesDialogComponent,
        FilterCommentsByType
    ],
    entryComponents: [RecipesFavouritesDialogComponent],
    imports: [
        CommonModule,
        RecipesSharedModule,
        MatStepperModule,
        MatFormFieldModule,
        RouterModule.forChild(recipesState),
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatSliderModule,
        MatSelectModule,
        NgxMatSelectSearchModule,
        MatIconModule,
        Ng5SliderModule,
        MatRadioModule,
        MatChipsModule,
        MatGridListModule,
        RatingModule,
        MatCardModule,
        MatSelectSearchModule,
        MatExpansionModule,
        MatDividerModule,
        MatDialogModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecipesModule {}
