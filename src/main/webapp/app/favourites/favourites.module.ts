import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipesSharedModule } from 'app/shared';
import { RouterModule } from '@angular/router';
import {
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatStepperModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { Ng5SliderModule } from 'ng5-slider';
import { RatingModule } from 'ng-starrating';
import { MatSelectSearchModule } from 'app/shared/select-with-search/mat-select-search.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDividerModule } from '@angular/material/divider';
import { favouritesState } from 'app/favourites/favourites.route';
import { FavouritesListComponent } from 'app/favourites/favourites-list/favourites-list.component';
import { FavouritesListDetailsComponent } from 'app/favourites/favourites-list/favourites-list-details/favourites-list-details.component';

@NgModule({
    declarations: [FavouritesListComponent, FavouritesListDetailsComponent],
    imports: [
        CommonModule,
        RecipesSharedModule,
        MatStepperModule,
        MatFormFieldModule,
        RouterModule.forChild(favouritesState),
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatSliderModule,
        MatSelectModule,
        NgxMatSelectSearchModule,
        MatIconModule,
        Ng5SliderModule,
        MatRadioModule,
        MatChipsModule,
        MatGridListModule,
        RatingModule,
        MatCardModule,
        MatSelectSearchModule,
        MatExpansionModule,
        MatDividerModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FavouritesModule {}
