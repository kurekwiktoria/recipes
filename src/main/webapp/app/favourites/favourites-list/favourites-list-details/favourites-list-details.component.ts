import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SERVER_API_URL } from 'app/app.constants';
import { FavouritesService } from 'app/core/service/favourites.service';
import { Favourite } from 'app/core/model/favourite.model';
import { Account, AccountService } from 'app/core';
import { Recipe } from 'app/core/model/recipe.model';

@Component({
    selector: 'jhi-favourites-list-details',
    templateUrl: './favourites-list-details.component.html',
    styles: [],
    styleUrls: ['./favourites-list-details.css']
})
export class FavouritesListDetailsComponent implements OnInit {
    @Input()
    favourite: Favourite;
    account: Account;
    downloadUrl = SERVER_API_URL + '/api/downloadFile/?path=';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private favouritesService: FavouritesService,
        private accountService: AccountService
    ) {}

    ngOnInit() {
        this.accountService.identity().then((account: Account) => {
            this.account = account;
        });
    }

    goToDetails(id: any) {
        this.router.navigate(['../przepisy/szczegoly/' + id], { relativeTo: this.route.parent });
    }

    removeFromList(recipe: Recipe) {
        this.favouritesService.removeRecipeFromFavourite(recipe.id, this.favourite.id).subscribe(() => {
            this.favourite.recipes.splice(this.favourite.recipes.indexOf(recipe), 1);
        });
    }
}
