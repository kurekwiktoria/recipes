import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FavouritesService } from 'app/core/service/favourites.service';
import { Favourite } from 'app/core/model/favourite.model';
import { Account, AccountService } from 'app/core';

@Component({
    selector: 'jhi-favourites-list',
    templateUrl: './favourites-list.component.html',
    styles: [],
    styleUrls: ['./favourites-list.css']
})
export class FavouritesListComponent implements OnInit {
    favourites: Favourite[];
    account: Account;
    selectedFavourite: Favourite = null;
    newListName: string;
    newListDescription = '';
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private favouritesService: FavouritesService,
        private accountService: AccountService
    ) {}

    ngOnInit() {
        this.accountService.identity().then((account: Account) => {
            this.account = account;
            this.favouritesService.findByUserLogin(this.account.login).subscribe(favs => {
                this.favourites = favs.body;
            });
        });
    }

    setSelected(favourite: Favourite) {
        this.selectedFavourite = favourite;
    }

    removeFavourite(fav: Favourite) {
        this.favouritesService.delete(fav.id).subscribe();
        this.favourites.splice(this.favourites.indexOf(fav), 1);
        this.selectedFavourite = null;
    }

    addNewList() {
        this.favouritesService
            .create(new Favourite(null, this.account.login, this.newListName, this.newListDescription, null))
            .subscribe(fav => {
                this.favourites.push(fav.body);
                this.newListName = '';
                this.newListDescription = '';
            });
    }
}
