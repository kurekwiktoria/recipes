import { Route } from '@angular/router';
import { FavouritesListComponent } from 'app/favourites/favourites-list/favourites-list.component';

export const favouritesListRoute: Route = {
    path: 'listy',
    component: FavouritesListComponent,
    data: {
        pageTitle: 'favourites.list.name'
    }
};
