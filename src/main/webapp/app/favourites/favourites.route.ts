import { Routes } from '@angular/router';
import { favouritesListRoute } from 'app/favourites/favourites-list/favourites-list.route';

const FAVOURITES_ROUTE = [favouritesListRoute];

export const favouritesState: Routes = [
    {
        path: '',
        children: FAVOURITES_ROUTE
    }
];
