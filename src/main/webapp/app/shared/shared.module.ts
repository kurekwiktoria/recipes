import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateMomentAdapter } from './util/datepicker-adapter';
import { HasAnyAuthorityDirective, JhiLoginModalComponent, RecipesSharedCommonModule, RecipesSharedLibsModule } from './';
import { HasNotAnyAuthorityDirective } from 'app/shared/auth/has-not-any-authority.directive';

@NgModule({
    imports: [RecipesSharedLibsModule, RecipesSharedCommonModule],
    declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective, HasNotAnyAuthorityDirective],
    providers: [{ provide: NgbDateAdapter, useClass: NgbDateMomentAdapter }],
    entryComponents: [JhiLoginModalComponent],
    exports: [RecipesSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective, HasNotAnyAuthorityDirective],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecipesSharedModule {
    static forRoot() {
        return {
            ngModule: RecipesSharedModule
        };
    }
}
