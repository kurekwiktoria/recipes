import { NgModule } from '@angular/core';
import { AddIngredientDialog, MatSelectSearchComponent } from './mat-select-search.component';
import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, MatButtonModule, MatIconModule, MatInputModule, FontAwesomeModule, MatDialogModule, FormsModule],
    declarations: [MatSelectSearchComponent, AddIngredientDialog],
    entryComponents: [AddIngredientDialog],
    exports: [MatButtonModule, MatInputModule, MatSelectSearchComponent]
})
export class MatSelectSearchModule {}
