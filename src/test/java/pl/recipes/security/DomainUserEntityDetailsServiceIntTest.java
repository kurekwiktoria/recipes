package pl.recipes.security;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.RecipesApp;
import pl.recipes.domain.UserEntity;
import pl.recipes.repository.UserRepository;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for DomainUserDetailsService.
 *
 * @see DomainUserDetailsService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RecipesApp.class)
@Transactional
public class DomainUserEntityDetailsServiceIntTest {

    private static final String USER_ONE_LOGIN = "test-user-one";
    private static final String USER_ONE_EMAIL = "test-user-one@localhost";
    private static final String USER_TWO_LOGIN = "test-user-two";
    private static final String USER_TWO_EMAIL = "test-user-two@localhost";
    private static final String USER_THREE_LOGIN = "test-user-three";
    private static final String USER_THREE_EMAIL = "test-user-three@localhost";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsService domainUserDetailsService;

    private UserEntity userEntityOne;
    private UserEntity userEntityTwo;
    private UserEntity userEntityThree;

    @Before
    public void init() {
        userEntityOne = new UserEntity();
        userEntityOne.setLogin(USER_ONE_LOGIN);
        userEntityOne.setPassword(RandomStringUtils.random(60));
        userEntityOne.setActivated(true);
        userEntityOne.setEmail(USER_ONE_EMAIL);
        userEntityOne.setFirstName("userEntityOne");
        userEntityOne.setLastName("doe");
        userEntityOne.setLangKey("en");
        userRepository.save(userEntityOne);

        userEntityTwo = new UserEntity();
        userEntityTwo.setLogin(USER_TWO_LOGIN);
        userEntityTwo.setPassword(RandomStringUtils.random(60));
        userEntityTwo.setActivated(true);
        userEntityTwo.setEmail(USER_TWO_EMAIL);
        userEntityTwo.setFirstName("userEntityTwo");
        userEntityTwo.setLastName("doe");
        userEntityTwo.setLangKey("en");
        userRepository.save(userEntityTwo);

        userEntityThree = new UserEntity();
        userEntityThree.setLogin(USER_THREE_LOGIN);
        userEntityThree.setPassword(RandomStringUtils.random(60));
        userEntityThree.setActivated(false);
        userEntityThree.setEmail(USER_THREE_EMAIL);
        userEntityThree.setFirstName("userEntityThree");
        userEntityThree.setLastName("doe");
        userEntityThree.setLangKey("en");
        userRepository.save(userEntityThree);
    }

    @Test
    @Transactional
    public void assertThatUserCanBeFoundByLogin() {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(USER_ONE_LOGIN);
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(USER_ONE_LOGIN);
    }

    @Test
    @Transactional
    public void assertThatUserCanBeFoundByLoginIgnoreCase() {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(USER_ONE_LOGIN.toUpperCase(Locale.ENGLISH));
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(USER_ONE_LOGIN);
    }

    @Test
    @Transactional
    public void assertThatUserCanBeFoundByEmail() {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(USER_TWO_EMAIL);
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(USER_TWO_LOGIN);
    }

    @Test(expected = UsernameNotFoundException.class)
    @Transactional
    public void assertThatUserCanNotBeFoundByEmailIgnoreCase() {
    domainUserDetailsService.loadUserByUsername(USER_TWO_EMAIL.toUpperCase(Locale.ENGLISH));
    }

    @Test
    @Transactional
    public void assertThatEmailIsPrioritizedOverLogin() {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(USER_ONE_EMAIL);
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(USER_ONE_LOGIN);
    }

    @Test(expected = UserNotActivatedException.class)
    @Transactional
    public void assertThatUserNotActivatedExceptionIsThrownForNotActivatedUsers() {
        domainUserDetailsService.loadUserByUsername(USER_THREE_LOGIN);
    }

}
