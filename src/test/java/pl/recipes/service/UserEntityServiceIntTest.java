package pl.recipes.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.recipes.RecipesApp;
import pl.recipes.config.Constants;
import pl.recipes.domain.UserEntity;
import pl.recipes.repository.UserRepository;
import pl.recipes.service.dto.UserDTO;
import pl.recipes.service.util.RandomUtil;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserService
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RecipesApp.class)
@Transactional
public class UserEntityServiceIntTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AuditingHandler auditingHandler;

    @Mock
    DateTimeProvider dateTimeProvider;

    private UserEntity userEntity;

    @Before
    public void init() {
        userEntity = new UserEntity();
        userEntity.setLogin("johndoe");
        userEntity.setPassword(RandomStringUtils.random(60));
        userEntity.setActivated(true);
        userEntity.setEmail("johndoe@localhost");
        userEntity.setFirstName("john");
        userEntity.setLastName("doe");
        userEntity.setImageUrl("http://placehold.it/50x50");
        userEntity.setLangKey("en");

        when(dateTimeProvider.getNow()).thenReturn(Optional.of(LocalDateTime.now()));
        auditingHandler.setDateTimeProvider(dateTimeProvider);
    }

    @Test
    @Transactional
    public void assertThatUserMustExistToResetPassword() {
        userRepository.saveAndFlush(userEntity);
        Optional<UserEntity> maybeUser = userService.requestPasswordReset("invalid.login@localhost");
        assertThat(maybeUser).isNotPresent();

        maybeUser = userService.requestPasswordReset(userEntity.getEmail());
        assertThat(maybeUser).isPresent();
        assertThat(maybeUser.orElse(null).getEmail()).isEqualTo(userEntity.getEmail());
        assertThat(maybeUser.orElse(null).getResetDate()).isNotNull();
        assertThat(maybeUser.orElse(null).getResetKey()).isNotNull();
    }

    @Test
    @Transactional
    public void assertThatOnlyActivatedUserCanRequestPasswordReset() {
        userEntity.setActivated(false);
        userRepository.saveAndFlush(userEntity);

        Optional<UserEntity> maybeUser = userService.requestPasswordReset(userEntity.getLogin());
        assertThat(maybeUser).isNotPresent();
        userRepository.delete(userEntity);
    }

    @Test
    @Transactional
    public void assertThatResetKeyMustNotBeOlderThan24Hours() {
        Instant daysAgo = Instant.now().minus(25, ChronoUnit.HOURS);
        String resetKey = RandomUtil.generateResetKey();
        userEntity.setActivated(true);
        userEntity.setResetDate(daysAgo);
        userEntity.setResetKey(resetKey);
        userRepository.saveAndFlush(userEntity);

        Optional<UserEntity> maybeUser = userService.completePasswordReset("johndoe2", userEntity.getResetKey());
        assertThat(maybeUser).isNotPresent();
        userRepository.delete(userEntity);
    }

    @Test
    @Transactional
    public void assertThatResetKeyMustBeValid() {
        Instant daysAgo = Instant.now().minus(25, ChronoUnit.HOURS);
        userEntity.setActivated(true);
        userEntity.setResetDate(daysAgo);
        userEntity.setResetKey("1234");
        userRepository.saveAndFlush(userEntity);

        Optional<UserEntity> maybeUser = userService.completePasswordReset("johndoe2", userEntity.getResetKey());
        assertThat(maybeUser).isNotPresent();
        userRepository.delete(userEntity);
    }

    @Test
    @Transactional
    public void assertThatUserCanResetPassword() {
        String oldPassword = userEntity.getPassword();
        Instant daysAgo = Instant.now().minus(2, ChronoUnit.HOURS);
        String resetKey = RandomUtil.generateResetKey();
        userEntity.setActivated(true);
        userEntity.setResetDate(daysAgo);
        userEntity.setResetKey(resetKey);
        userRepository.saveAndFlush(userEntity);

        Optional<UserEntity> maybeUser = userService.completePasswordReset("johndoe2", userEntity.getResetKey());
        assertThat(maybeUser).isPresent();
        assertThat(maybeUser.orElse(null).getResetDate()).isNull();
        assertThat(maybeUser.orElse(null).getResetKey()).isNull();
        assertThat(maybeUser.orElse(null).getPassword()).isNotEqualTo(oldPassword);

        userRepository.delete(userEntity);
    }

    @Test
    @Transactional
    public void testFindNotActivatedUsersByCreationDateBefore() {
        Instant now = Instant.now();
        when(dateTimeProvider.getNow()).thenReturn(Optional.of(now.minus(4, ChronoUnit.DAYS)));
        userEntity.setActivated(false);
        UserEntity dbUserEntity = userRepository.saveAndFlush(userEntity);
        dbUserEntity.setCreatedDate(now.minus(4, ChronoUnit.DAYS));
        userRepository.saveAndFlush(userEntity);
        List<UserEntity> userEntities = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minus(3, ChronoUnit.DAYS));
        assertThat(userEntities).isNotEmpty();
        userService.removeNotActivatedUsers();
        userEntities = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minus(3, ChronoUnit.DAYS));
        assertThat(userEntities).isEmpty();
    }

    @Test
    @Transactional
    public void assertThatAnonymousUserIsNotGet() {
        userEntity.setLogin(Constants.ANONYMOUS_USER);
        if (!userRepository.findOneByLogin(Constants.ANONYMOUS_USER).isPresent()) {
            userRepository.saveAndFlush(userEntity);
        }
        final PageRequest pageable = PageRequest.of(0, (int) userRepository.count());
        final Page<UserDTO> allManagedUsers = userService.getAllManagedUsers(pageable);
        assertThat(allManagedUsers.getContent().stream()
            .noneMatch(user -> Constants.ANONYMOUS_USER.equals(user.getLogin())))
            .isTrue();
    }


    @Test
    @Transactional
    public void testRemoveNotActivatedUsers() {
        // custom "now" for audit to use as creation date
        when(dateTimeProvider.getNow()).thenReturn(Optional.of(Instant.now().minus(30, ChronoUnit.DAYS)));

        userEntity.setActivated(false);
        userRepository.saveAndFlush(userEntity);

        assertThat(userRepository.findOneByLogin("johndoe")).isPresent();
        userService.removeNotActivatedUsers();
        assertThat(userRepository.findOneByLogin("johndoe")).isNotPresent();
    }

}
