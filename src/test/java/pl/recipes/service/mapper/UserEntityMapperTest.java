package pl.recipes.service.mapper;


import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.recipes.RecipesApp;
import pl.recipes.domain.UserEntity;
import pl.recipes.service.dto.UserDTO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the UserMapper.
 *
 * @see UserMapper
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RecipesApp.class)
public class UserEntityMapperTest {

    private static final String DEFAULT_LOGIN = "johndoe";

    @Autowired
    private UserMapper userMapper;

    private UserEntity userEntity;
    private UserDTO userDto;

    private static final Long DEFAULT_ID = 1L;

    @Before
    public void init() {
        userEntity = new UserEntity();
        userEntity.setLogin(DEFAULT_LOGIN);
        userEntity.setPassword(RandomStringUtils.random(60));
        userEntity.setActivated(true);
        userEntity.setEmail("johndoe@localhost");
        userEntity.setFirstName("john");
        userEntity.setLastName("doe");
        userEntity.setImageUrl("image_url");
        userEntity.setLangKey("en");

        userDto = new UserDTO(userEntity);
    }

    @Test
    public void usersToUserDTOsShouldMapOnlyNonNullUsers(){
        List<UserEntity> userEntities = new ArrayList<>();
        userEntities.add(userEntity);
        userEntities.add(null);

        List<UserDTO> userDTOS = userMapper.usersToUserDTOs(userEntities);

        assertThat(userDTOS).isNotEmpty();
        assertThat(userDTOS).size().isEqualTo(1);
    }

    @Test
    public void userDTOsToUsersShouldMapOnlyNonNullUsers(){
        List<UserDTO> usersDto = new ArrayList<>();
        usersDto.add(userDto);
        usersDto.add(null);

        List<UserEntity> userEntities = userMapper.userDTOsToUsers(usersDto);

        assertThat(userEntities).isNotEmpty();
        assertThat(userEntities).size().isEqualTo(1);
    }

    @Test
    public void userDTOsToUsersWithAuthoritiesStringShouldMapToUsersWithAuthoritiesDomain(){
        Set<String> authoritiesAsString = new HashSet<>();
        authoritiesAsString.add("ADMIN");
        userDto.setAuthorities(authoritiesAsString);

        List<UserDTO> usersDto = new ArrayList<>();
        usersDto.add(userDto);

        List<UserEntity> userEntities = userMapper.userDTOsToUsers(usersDto);

        assertThat(userEntities).isNotEmpty();
        assertThat(userEntities).size().isEqualTo(1);
        assertThat(userEntities.get(0).getAuthorities()).isNotNull();
        assertThat(userEntities.get(0).getAuthorities()).isNotEmpty();
        assertThat(userEntities.get(0).getAuthorities().iterator().next().getName()).isEqualTo("ADMIN");
    }

    @Test
    public void userDTOsToUsersMapWithNullAuthoritiesStringShouldReturnUserWithEmptyAuthorities(){
        userDto.setAuthorities(null);

        List<UserDTO> usersDto = new ArrayList<>();
        usersDto.add(userDto);

        List<UserEntity> userEntities = userMapper.userDTOsToUsers(usersDto);

        assertThat(userEntities).isNotEmpty();
        assertThat(userEntities).size().isEqualTo(1);
        assertThat(userEntities.get(0).getAuthorities()).isNotNull();
        assertThat(userEntities.get(0).getAuthorities()).isEmpty();
    }

    @Test
    public void userDTOToUserMapWithAuthoritiesStringShouldReturnUserWithAuthorities(){
        Set<String> authoritiesAsString = new HashSet<>();
        authoritiesAsString.add("ADMIN");
        userDto.setAuthorities(authoritiesAsString);

        userDto.setAuthorities(authoritiesAsString);

        UserEntity userEntity = userMapper.userDTOToUser(userDto);

        assertThat(userEntity).isNotNull();
        assertThat(userEntity.getAuthorities()).isNotNull();
        assertThat(userEntity.getAuthorities()).isNotEmpty();
        assertThat(userEntity.getAuthorities().iterator().next().getName()).isEqualTo("ADMIN");
    }

    @Test
    public void userDTOToUserMapWithNullAuthoritiesStringShouldReturnUserWithEmptyAuthorities(){
        userDto.setAuthorities(null);

        UserEntity userEntity = userMapper.userDTOToUser(userDto);

        assertThat(userEntity).isNotNull();
        assertThat(userEntity.getAuthorities()).isNotNull();
        assertThat(userEntity.getAuthorities()).isEmpty();
    }

    @Test
    public void userDTOToUserMapWithNullUserShouldReturnNull(){
        assertThat(userMapper.userDTOToUser(null)).isNull();
    }

    @Test
    public void testUserFromId() {
        assertThat(userMapper.userFromId(DEFAULT_ID).getId()).isEqualTo(DEFAULT_ID);
        assertThat(userMapper.userFromId(null)).isNull();
    }
}
